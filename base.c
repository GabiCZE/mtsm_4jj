/**
 * Project: Parallel solution of PDEs using Method of Lines (MOL)
 * The MOL discretizes one dimension and then integrates the semi-discrete problem as a system of ODEs.
 * The resulting system of ODEs can be solved using standard numerical methods - MTSM and RK solvers.
 * File: base.c
 * Author: Gabriela Necasova, inecasova@fit.vutbr.cz
 * Date: August 2018
 * Modification: November 2020
 */

//static char help[] = "Solves a system of ODEs using Modern Taylor Series Method (MTSM)\n";


/*
  Include "petscksp.h"  Note that this file automatically includes:
     petscsys.h       - base PETSc routines      petscvec.h - vectors
     petscmat.h       - matrices
     petscis.h        - index sets            	 petscksp.h - Krylov subspace methods
     petscviewer.h    - viewers                  petscpc.h  - preconditioners
*/


#include <petscksp.h>
#include <petsctime.h>
#include <petscts.h>

#include <stdio.h>

#include "general.h"
#include "datatypes.h"

// ======================================================================
/**
 * o2:
 * - uses only the last calculated Taylor series term
 * - do not use ADYnorm
 * Solves the system of ODEs numerically using explicit Taylor series method.
 * Definition of the problem: y' = A*y + b
 * where A is Jacobian matrix of constants and b is a vector of
 * constants and y is a vector [U;I]
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode ExplicitTaylor_o2(TParams *params, TData *data)
{

	PetscFunctionBeginUser;

	MPI_Comm       	comm;			// MPI communicator
	PetscMPIInt 	rank; 			// rank of the process
	PetscMPIInt 	size; 			// size of the group associated with a communicator
	PetscErrorCode 	ierr;

	/*****************************************
	 *  Modern Taylor Series Method (MTSM) - linear system of ODEs
	 *****************************************/
	// vectors, matrices
	Vec 			ADY;			// ADY = A * DY(k-1,:), and also DY(k,:) = (dt/k) * ADY
	//Vec 			maxDY;			// maximal DY tolerance
	Vec 			DYkPrev;    	// previous Taylor series term
	Vec				Y;  			// solution
	Vec				Yprev; 			// previous result of the timestep
							 		// stopping rule: stopping rule: the absolute values of three successive MTSM terms are
  									// less than the required accuracy (eps)
	Vec ORDvec;

	#ifdef OUTPUT
		Vec T;
	#endif
	// -----------------

	// default values
	PetscScalar    	ls_tol = 1e-11;     // tolerance in the last step
	//PetscScalar  	max_DY = 1e15;		// maximum order of the explicit Taylor series method
	//const PetscInt  stopping = 3;       // stopping rule (number of TS terms which have to be smaller then eps)

	// variables
	//PetscReal 		norm;           // norm - in condition of inner while (while norm > eps)
	//PetscReal 		ADYNorm = 1e15;        // norm - for checking maxORD (inner while)

	PetscScalar 	t = 0.0; 		// simulation time
	PetscScalar  	ORD = 0.0; 		// order of the method in each timestep
	PetscScalar dt = params->dt; 	// integration step

	// indeces
	PetscInt       	i = 0;		// index for timestep
	PetscInt       	k = 0; 		// index for TS terms
	PetscReal 		maxval = 1e15;		// maximum value of the vector
	PetscReal 		maxAbsval = 1e15;

	comm = PETSC_COMM_WORLD;	 // equivalent of the MPI_COMM_WORLD communicator which represents all the processs that PETSc knows about


	////////////////////////////////////////////////////////
	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
	// size of the group associated with a communicator
	ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);

	////////////////////////////////////////////////////////
	// vector DYkPrev
	ierr = VecCreate(comm,&DYkPrev);CHKERRQ(ierr);
	ierr = VecSetType(DYkPrev,VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(DYkPrev,PETSC_DECIDE,data->data_size);CHKERRQ(ierr);
	ierr = VecSet(DYkPrev,0);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(DYkPrev);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(DYkPrev);CHKERRQ(ierr);

	// vector ADY
	ierr = VecDuplicate(DYkPrev,&ADY);CHKERRQ(ierr);
	// vector Y
	ierr = VecDuplicate(DYkPrev,&Y);CHKERRQ(ierr);
	// vector Yprev
	ierr = VecDuplicate(DYkPrev,&Yprev);CHKERRQ(ierr);

	// vector maxDY
	/*ierr = VecCreate(comm,&maxDY);CHKERRQ(ierr);
	ierr = VecSetType(maxDY, VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(maxDY,PETSC_DECIDE,stopping);CHKERRQ(ierr);
	ierr = VecSetOption(maxDY,VEC_IGNORE_NEGATIVE_INDICES,PETSC_TRUE);CHKERRQ(ierr);
	ierr = VecSet(maxDY,0);CHKERRQ(ierr);*/

	////////////////////////////////////////////////////////////
	// set ORDvec
	ierr = VecCreate(comm,&ORDvec);CHKERRQ(ierr);
	ierr = VecSetType(ORDvec,VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(ORDvec,PETSC_DECIDE,1);CHKERRQ(ierr);
	ierr = VecSet(ORDvec,ORD);CHKERRQ(ierr);

	#ifdef OUTPUT
		// ORD
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);

		// T
		ierr = VecCreate(comm,&T);CHKERRQ(ierr);
		ierr = VecSetType(T,VECMPI);CHKERRQ(ierr);
		ierr = VecSetSizes(T,PETSC_DECIDE,1);CHKERRQ(ierr);
		ierr = VecSet(T,t);CHKERRQ(ierr);
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(T,params->viewerT);CHKERRQ(ierr);
	#endif

	////////////////////////////////////////////////////////////
	// computation
	////////////////////////////////////////////////////////////

	// START MEASURING TIME
	#ifdef TIME
		PetscLogDouble start, end;
		PetscTime(&start);
	#endif

	//////////////////////////////////////////////
	// MATLAB: y(1,:) = init;
	ierr = VecCopy(data->ic,Yprev);CHKERRQ(ierr);

	ierr = VecCopy(data->ic,Y);CHKERRQ(ierr);

	#ifdef OUTPUT
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);
	#endif

	/*#ifdef LOGVIEW
		//PetscPrintf(PETSC_COMM_WORLD, "LOGVIEW ON\n");
		PetscPreLoadBegin(PETSC_FALSE, "MTSM begin");
	#endif*/

	// timestep index
	i = 2;

	/*#ifdef LOGVIEW
		PetscPreLoadStage("MTSM time loop");
	#endif*/

	// MATLAB: while (t + ls_tol < tmax)
	while(t + ls_tol < params->tmax)
	{
		/*#ifdef LOGVIEW
			char label[100];

		    sprintf(label, "MTSM step_%d", i); // puts string into buffer
		    //printf("%s\n", label); // outputs so you can see it

			PetscPreLoadStage(label);
		#endif*/

		// MATLAB: DY = zeros(maxORD, ne);
		ierr = VecZeroEntries(DYkPrev);CHKERRQ(ierr);

		// MATLAB: y(i,:) = y(i-1,:)
		// PETSc: Y = Yprev
		// y <- x
		// VecCopy(Vec x,Vec y)
		ierr = VecCopy(Yprev,Y);CHKERRQ(ierr);

		// MATLAB: Ay = A*y(i,:)+b;
		// PETSc: DYkPrev = A*Y + b
		// v3 = v2 + A * v1
		// MatMultAdd(Mat mat,Vec v1,Vec v2,Vec v3)
		ierr = MatMultAdd(data->A,Y,data->b,DYkPrev);CHKERRQ(ierr);

		// MATLAB: DY(1,:) = h*(Ay+b);
		// PETSc: DYkPrev = DYkPrev * dt
		// x[i] = alpha * x[i], for i=1,...,n
		// VecScale(Vec x, PetscScalar alpha)
		ierr = VecScale(DYkPrev, dt);CHKERRQ(ierr);

		// MATLAB: y(i,:) = y(i,:) + DY(1,:);
		// PETSc: Y = 1*DYkPRev + Y
		// y = alpha x + y
		// VecAXPY(Vec y,PetscScalar alpha,Vec x)
		// NOTE: that's ok because multiplying by 1 is not performed (internal implementation)
		ierr = VecAXPY(Y,1.0,DYkPrev);CHKERRQ(ierr);

		// MATLAB: maxDY(1:stopping) = ones(1,stopping)*10^10;
		// PETSc: maxDY[1:stopping] = 1e10
		//ierr = VecSet(maxDY, 1e10);CHKERRQ(ierr);

		// PETSc: ADYnorm = VecNorm(DYkPrev)
		//ierr = VecNorm(DYkPrev,NORM_2,&ADYNorm);CHKERRQ(ierr);

		ierr = VecMax(DYkPrev,NULL,&maxval);CHKERRQ(ierr);
		maxAbsval = PetscAbsReal(maxval);

		k = 2;


		//////////////////////////////////////////
		// INNER WHILE CYCLE - other Taylor series terms
		// MATLAB: while(norm(maxDY(k-1:stopping+k-2)))>eps
		// PETSc: norm = VecNorm(maxDY)
		// Note: Eucledian norm (2-norm): sqrt(sum_i |x_i|^2)

		//ierr = VecView(PETSC_VIEWER_STDOUT_WORLD, norm);
		//ierr = VecNorm(maxDY,NORM_2,&norm);CHKERRQ(ierr);

		/*#ifdef LOGVIEW
			PetscPreLoadStage("MTSM DY");
		#endif*/


		/*#ifdef LOGVIEW
			char labelDY[100];

		    sprintf(labelDY, "MTSM DY terms_%d", k); // puts string into buffer
		    //printf("%s\n", label); // outputs so you can see it

			PetscPreLoadStage(labelDY);
		#endif*/

		while(maxAbsval > params->eps)
		{
			// MATLAB: ADy = A*DY(k-1,:)';
			// PETSc: ADY = A*DykPrev
			// MatMult(A,Vec x,Vec y);
			// y = Ax
			ierr = MatMult(data->A,DYkPrev,ADY);CHKERRQ(ierr);

//			ierr = PetscPrintf(comm,"i=%d, k=%d,  ADYnorm: %g\n", i, k, ADYNorm);CHKERRQ(ierr);


			// MATLAB: DY(k,:) = (h/k)*ADy;
			// PETSc: ADY = ADy * (dt/k)
			ierr = VecScale(ADY, (dt/(k)));CHKERRQ(ierr);

			// PETSc: ADYnorm = VecNorm(ADY)
			//ierr = VecNorm(ADY,NORM_2,&ADYNorm);CHKERRQ(ierr);

			// MATLAB: y(i,:) = y(i,:) + DY(k,:);
			// PETSc: Y = 1*Y + ADY
			// y = alpha x + y
			// VecAXPY(Vec y,PetscScalar alpha,Vec x)
			ierr = VecAXPY(Y,1.0,ADY);CHKERRQ(ierr);

// ZAKOMENTOVANE:
			// MATLAB: maxDY(k+stopping-1) = abs(max(DY(k,:)));
			// PETSc: maxval = max(ADY)

			ierr = VecMax(ADY,NULL,&maxval);CHKERRQ(ierr);
			maxAbsval = PetscAbsReal(maxval);
/////////////////////

	  		// MATLAB: maxDY(mod(k,3)+1)=abs(max(DY(:,k)));
			/*ierr = VecSetValue(maxDY,(k % stopping),PetscAbsReal(maxval),INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecAssemblyBegin(maxDY);CHKERRQ(ierr);
			ierr = VecAssemblyEnd(maxDY);CHKERRQ(ierr);*/

			// MATLAB: k = k + 1
			k = k + 1;

			// PETSc: DYkPrev = ADY; (note: ADY == DY in MATLAB)
			ierr = VecCopy(ADY,DYkPrev);CHKERRQ(ierr);

			// MATLAB: norm(maxDY(k-1:stopping+k-2))
			// PETSc: norm = VecNorm(maxDY)
			// note: to be ready for next while iteration
			//ierr = VecNorm(maxDY,NORM_2,&norm);CHKERRQ(ierr);

			// MATLAB: if (k > maxORD) || (norm(ADy) > max_DY)
			if(k > params->maxORD)
			{
				//ierr = PetscPrintf(comm,"maxORD reached!\n", dt);CHKERRQ(ierr);
				break;
			}

			#ifdef DEBUG
				ierr = PetscPrintf(comm,"TS TERMS: i=%d, k=%d, maxAbsval=%g\n", i, k-1, maxAbsval);CHKERRQ(ierr);
				ierr = VecView(ADY, PETSC_VIEWER_STDOUT_WORLD);
				ierr = PetscPrintf(comm,"----------\n");CHKERRQ(ierr);
			#endif
		}//end inner while

		///////////////////////////////////////////////////////////
		// MATLAB: if (k > maxORD) || (norm(ADy) > max_DY)
	  	//if(k > MAXORD || ADYNorm > max_DY)
		if(k > params->maxORD)
		{
			dt = dt/2;
			#ifdef DEBUG
				ierr = PetscPrintf(comm,"Halving! dt = %g t = %g\n", dt, t);CHKERRQ(ierr);
			#endif
		}
		else
		{
	  		// save order of the method in current timestep
			ORD = ORD + k;

			// shift simulation time
			t = t + dt;
			i = i + 1;

			// PETSc: Yprev = Y;
			ierr = VecCopy(Y,Yprev);CHKERRQ(ierr);

			#ifdef OUTPUT
				// WRITE: write vectors to the binary file (for MATLAB)
				ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);

				ierr = VecSet(T,t);CHKERRQ(ierr);
				ierr = VecView(T,params->viewerT);CHKERRQ(ierr);

				ierr = VecSet(ORDvec,k);CHKERRQ(ierr);
				ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);
			#endif

			#ifdef DEBUG
				ierr = VecSet(ORDvec,k);CHKERRQ(ierr);
				//ierr = PetscPrintf(comm,"RESULTS: i=%d\n", i-1);CHKERRQ(ierr);
				//ierr = VecView(Y, PETSC_VIEWER_STDOUT_WORLD);
				//ierr = PetscPrintf(comm,"==========\n");CHKERRQ(ierr);
			#endif
		}

	}// end outer while

	#ifdef DEBUG
		ierr = PetscPrintf(comm,"avg_ORD %f: %f / %d\n", (ORD/(i-1)), ORD, (i-1));CHKERRQ(ierr);
		PetscScalar maxord;
		PetscScalar minord;
		ierr = VecMax(ORDvec,NULL,&maxord);CHKERRQ(ierr);
		ierr = PetscPrintf(comm,"max_ORD: %f\n", maxord);CHKERRQ(ierr);
		ierr = VecMin(ORDvec,NULL,&minord);CHKERRQ(ierr);
		ierr = PetscPrintf(comm,"min_ORD: %f\n", minord);CHKERRQ(ierr);
	#endif
	// TIME: print only results at dt = tmax (we do not store vector T)
	// write vectors to the binary file (for MATLAB)
	// we only open bin file here and then close it immediately
	#ifdef TIME
		// STOP MEASURING TIME
		PetscTime(&end);

		ierr = PetscPrintf(PETSC_COMM_WORLD,"MTSM_O2,N:%d,proc:%d,time:%e,steps:%d,precalc-time:0,comp-time:0,avg-ORD:%f,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n",
			params->N, size, (end-start), i, (ORD/(i-1)), data->data_size, data->info_global_sum.nz_allocated, data->info_global_sum.nz_used,data->glob_sum_nz_used_percent,data->info_global_sum.memory);CHKERRQ(ierr);

		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);

		// calculate the average ORD
		ORD = ORD/i;
		ierr = VecSet(ORDvec,ORD);CHKERRQ(ierr);
		ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);

	#endif

	/////////////////////////////////////////////////////////////
	/*#ifdef LOGVIEW
		PetscPreLoadEnd();
	#endif*/

	// destroy vectors
	#ifdef OUTPUT
		ierr = VecDestroy(&T);CHKERRQ(ierr);
	#endif

	ierr = VecDestroy(&ADY);CHKERRQ(ierr);
	ierr = VecDestroy(&ORDvec);CHKERRQ(ierr);
	ierr = VecDestroy(&DYkPrev);CHKERRQ(ierr);
	ierr = VecDestroy(&Y);CHKERRQ(ierr);
	ierr = VecDestroy(&Yprev);CHKERRQ(ierr);
	//ierr = VecDestroy(&maxDY);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * o1: uses only 1 stopping array (maxDY)
 * Solves the system of ODEs numerically using explicit Taylor series method.
 * Definition of the problem: y' = A*y + b
 * where A is Jacobian matrix of constants and b is a vector of
 * constants and y is a vector [U;I]
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode ExplicitTaylor_o1(TParams *params, TData *data)
{

	PetscFunctionBeginUser;

	MPI_Comm       	comm;			// MPI communicator
	PetscMPIInt 	rank; 			// rank of the process
	PetscMPIInt 	size; 			// size of the group associated with a communicator
	PetscErrorCode 	ierr;

	/*****************************************
	 *  Modern Taylor Series Method (MTSM) - linear system of ODEs
	 *****************************************/
	// vectors, matrices
	Vec 			ADY;			// ADY = A * DY(k-1,:), and also DY(k,:) = (dt/k) * ADY
	Vec 			maxDY;			// maximal DY tolerance
	Vec 			DYkPrev;    	// previous Taylor series term
	Vec				Y;  			// solution
	Vec				Yprev; 			// previous result of the timestep
							 		// stopping rule: stopping rule: the absolute values of three successive MTSM terms are
  									// less than the required accuracy (eps)
	Vec ORDvec;

	#ifdef OUTPUT
		Vec T;
	#endif
	// -----------------

	// default values
	PetscScalar    	ls_tol = 1e-11;     // tolerance in the last step
	PetscScalar  	max_DY = 1e15;		// maximum order of the explicit Taylor series method
	const PetscInt  stopping = 3;       // stopping rule (number of TS terms which have to be smaller then eps)

	// variables
	PetscReal 		norm;           // norm - in condition of inner while (while norm > eps)
	PetscReal 		ADYNorm;        // norm - for checking maxORD (inner while)

	PetscScalar 	t = 0.0; 		// simulation time
	PetscScalar  	ORD = 0.0; 		// order of the method in each timestep
	PetscScalar dt = params->dt; 	// integration step

	// indeces
	PetscInt       	i = 0;		// index for timestep
	PetscInt       	k = 0; 		// index for TS terms
	PetscReal 		maxval;		// maximum value of the vector

	comm = PETSC_COMM_WORLD;	 // equivalent of the MPI_COMM_WORLD communicator which represents all the processs that PETSc knows about


	////////////////////////////////////////////////////////
	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
	// size of the group associated with a communicator
	ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);

	////////////////////////////////////////////////////////
	// vector DYkPrev
	ierr = VecCreate(comm,&DYkPrev);CHKERRQ(ierr);
	ierr = VecSetType(DYkPrev,VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(DYkPrev,PETSC_DECIDE,data->data_size);CHKERRQ(ierr);
	ierr = VecSet(DYkPrev,0);CHKERRQ(ierr);
	ierr = VecAssemblyBegin(DYkPrev);CHKERRQ(ierr);
	ierr = VecAssemblyEnd(DYkPrev);CHKERRQ(ierr);

	// vector ADY
	ierr = VecDuplicate(DYkPrev,&ADY);CHKERRQ(ierr);
	// vector Y
	ierr = VecDuplicate(DYkPrev,&Y);CHKERRQ(ierr);
	// vector Yprev
	ierr = VecDuplicate(DYkPrev,&Yprev);CHKERRQ(ierr);

	// vector maxDY
	ierr = VecCreateSeq(PETSC_COMM_SELF ,stopping,&maxDY);CHKERRQ(ierr);

	//ierr = VecCreate(comm,&maxDY);CHKERRQ(ierr);
	//ierr = VecSetType(maxDY, VECMPI);CHKERRQ(ierr);
	//ierr = VecSetSizes(maxDY,PETSC_DECIDE,stopping);CHKERRQ(ierr);

	// NEW: maxDY is sequential vector
	//ierr = VecSetType(maxDY, VECSEQ);CHKERRQ(ierr);
	//ierr = VecSetSizes(maxDY,stopping,stopping);CHKERRQ(ierr);

	//ierr = VecSetOption(maxDY,VEC_IGNORE_NEGATIVE_INDICES,PETSC_TRUE);CHKERRQ(ierr);
	ierr = VecSet(maxDY,0);CHKERRQ(ierr);

	////////////////////////////////////////////////////////////
	// set ORDvec
	ierr = VecCreate(comm,&ORDvec);CHKERRQ(ierr);
	ierr = VecSetType(ORDvec,VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(ORDvec,PETSC_DECIDE,1);CHKERRQ(ierr);
	ierr = VecSet(ORDvec,ORD);CHKERRQ(ierr);

	#ifdef OUTPUT
		// ORD
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);

		// T
		ierr = VecCreate(comm,&T);CHKERRQ(ierr);
		ierr = VecSetType(T,VECMPI);CHKERRQ(ierr);
		ierr = VecSetSizes(T,PETSC_DECIDE,1);CHKERRQ(ierr);
		ierr = VecSet(T,t);CHKERRQ(ierr);
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(T,params->viewerT);CHKERRQ(ierr);
	#endif

	////////////////////////////////////////////////////////////
	// computation
	////////////////////////////////////////////////////////////

	// START MEASURING TIME
	#ifdef TIME
		PetscLogDouble start, end;
		PetscTime(&start);
	#endif

	//////////////////////////////////////////////
	// MATLAB: y(1,:) = init;
	ierr = VecCopy(data->ic,Yprev);CHKERRQ(ierr);

	ierr = VecCopy(data->ic,Y);CHKERRQ(ierr);

	#ifdef OUTPUT
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);
	#endif

	/*#ifdef LOGVIEW
		//PetscPrintf(PETSC_COMM_WORLD, "LOGVIEW ON\n");
		PetscPreLoadBegin(PETSC_FALSE, "MTSM begin");
	#endif*/

	// timestep index
	i = 2;

	/*#ifdef LOGVIEW
		PetscPreLoadStage("MTSM time loop");
	#endif*/

	// MATLAB: while (t + ls_tol < tmax)
	while(t + ls_tol < params->tmax)
	{
		/*#ifdef LOGVIEW
			char label[100];

		    sprintf(label, "MTSM step_%d", i); // puts string into buffer
		    //printf("%s\n", label); // outputs so you can see it

			PetscPreLoadStage(label);
		#endif*/

		// MATLAB: DY = zeros(maxORD, ne);
		ierr = VecZeroEntries(DYkPrev);CHKERRQ(ierr);

		// MATLAB: y(i,:) = y(i-1,:)
		// PETSc: Y = Yprev
		// y <- x
		// VecCopy(Vec x,Vec y)
		ierr = VecCopy(Yprev,Y);CHKERRQ(ierr);

		// MATLAB: Ay = A*y(i,:)+b;
		// PETSc: DYkPrev = A*Y + b
		// v3 = v2 + A * v1
		// MatMultAdd(Mat mat,Vec v1,Vec v2,Vec v3)
		ierr = MatMultAdd(data->A,Y,data->b,DYkPrev);CHKERRQ(ierr);

		// MATLAB: DY(1,:) = h*(Ay+b);
		// PETSc: DYkPrev = DYkPrev * dt
		// x[i] = alpha * x[i], for i=1,...,n
		// VecScale(Vec x, PetscScalar alpha)
		ierr = VecScale(DYkPrev, dt);CHKERRQ(ierr);

		// MATLAB: y(i,:) = y(i,:) + DY(1,:);
		// PETSc: Y = 1*DYkPRev + Y
		// y = alpha x + y
		// VecAXPY(Vec y,PetscScalar alpha,Vec x)
		// NOTE: that's ok because multiplying by 1 is not performed (internal implementation)
		ierr = VecAXPY(Y,1.0,DYkPrev);CHKERRQ(ierr);

		// MATLAB: maxDY(1:stopping) = ones(1,stopping)*10^10;
		// PETSc: maxDY[1:stopping] = 1e10
		ierr = VecSet(maxDY, 1e10);CHKERRQ(ierr);

		k = 2;

		//////////////////////////////////////////
		// INNER WHILE CYCLE - other Taylor series terms
		// MATLAB: while(norm(maxDY(k-1:stopping+k-2)))>eps
		// PETSc: norm = VecNorm(maxDY)
		// Note: Eucledian norm (2-norm): sqrt(sum_i |x_i|^2)

		//ierr = VecView(PETSC_VIEWER_STDOUT_WORLD, norm);
		ierr = VecNorm(maxDY,NORM_2,&norm);CHKERRQ(ierr);

		/*#ifdef LOGVIEW
			PetscPreLoadStage("MTSM DY");
		#endif*/


		/*#ifdef LOGVIEW
			char labelDY[100];

		    sprintf(labelDY, "MTSM DY terms_%d", k); // puts string into buffer
		    //printf("%s\n", label); // outputs so you can see it

			PetscPreLoadStage(labelDY);
		#endif*/

		while(norm > params->eps)
		{

			// MATLAB: ADy = A*DY(k-1,:)';
			// PETSc: ADY = A*DykPrev
			// MatMult(A,Vec x,Vec y);
			// y = Ax
			ierr = MatMult(data->A,DYkPrev,ADY);CHKERRQ(ierr);

			// PETSc: ADYnorm = VecNorm(ADY)
			ierr = VecNorm(ADY,NORM_2,&ADYNorm);CHKERRQ(ierr);

			// MATLAB: DY(k,:) = (h/k)*ADy;
			// PETSc: ADY = ADy * (dt/k)
			ierr = VecScale(ADY, (dt/(k)));CHKERRQ(ierr);

			// MATLAB: y(i,:) = y(i,:) + DY(k,:);
			// PETSc: Y = 1*Y + ADY
			// y = alpha x + y
			// VecAXPY(Vec y,PetscScalar alpha,Vec x)
			ierr = VecAXPY(Y,1.0,ADY);CHKERRQ(ierr);

			// MATLAB: maxDY(k+stopping-1) = abs(max(DY(k,:)));
			// PETSc: maxval = max(ADY)
			ierr = VecMax(ADY,NULL,&maxval);CHKERRQ(ierr);
			//ierr = VecView(ADY, PETSC_VIEWER_STDOUT_WORLD);

			// MATLAB: norm(maxDY(k-1:stopping+k-2))
			// PETSc: norm = VecNorm(maxDY)
			// note: to be ready for next while iteration
			ierr = VecNorm(maxDY,NORM_2,&norm);CHKERRQ(ierr);

			// MATLAB: k = k + 1
			k = k + 1;

	  		// MATLAB: maxDY(mod(k,3)+1)=abs(max(DY(:,k)));
			ierr = VecSetValue(maxDY,(k % stopping),PetscAbsReal(maxval),INSERT_VALUES);CHKERRQ(ierr);
			ierr = VecAssemblyBegin(maxDY);CHKERRQ(ierr);
			ierr = VecAssemblyEnd(maxDY);CHKERRQ(ierr);

			// PETSc: DYkPrev = ADY; (note: ADY == DY in MATLAB)
			ierr = VecCopy(ADY,DYkPrev);CHKERRQ(ierr);

			// WARNING!!!!!!!!!!
			// MATLAB: norm(maxDY(k-1:stopping+k-2))
			// PETSc: norm = VecNorm(maxDY)
			// note: to be ready for next while iteration
			//ierr = VecNorm(maxDY,NORM_2,&norm);CHKERRQ(ierr);

			// MATLAB: k = k + 1
			//k = k + 1;

			// MATLAB: if (k > maxORD) || (norm(ADy) > max_DY)
			if(k > params->maxORD || ADYNorm > max_DY)
			{
				//ierr = PetscPrintf(comm,"maxORD reached!\n", dt);CHKERRQ(ierr);
				break;
			}

			#ifdef DEBUG
				//ierr = PetscPrintf(comm,"TS TERMS: i=%d, k=%d, norm=%g\n", i, k-1, norm);CHKERRQ(ierr);
				//ierr = VecView(ADY, PETSC_VIEWER_STDOUT_WORLD);
				//ierr = PetscPrintf(comm,"----------\n");CHKERRQ(ierr);
			#endif
		}//end inner while

		///////////////////////////////////////////////////////////
		// MATLAB: if (k > maxORD) || (norm(ADy) > max_DY)
	  	//if(k > MAXORD || ADYNorm > max_DY)
		if(k > params->maxORD || ADYNorm > max_DY)
		{
			dt = dt/2;
			#ifdef DEBUG
				ierr = PetscPrintf(comm,"Halving! dt = %g t = %g\n", dt, t);CHKERRQ(ierr);
			#endif
		}
		else
		{
	  		// save order of the method in current timestep
			ORD = ORD + (k-1);

			#ifdef DEBUG
				//ierr = PetscPrintf(comm,"k: %d\n", k);CHKERRQ(ierr);
			#endif

			// shift simulation time
			t = t + dt;
			i = i + 1;

			// PETSc: Yprev = Y;
			ierr = VecCopy(Y,Yprev);CHKERRQ(ierr);

			#ifdef OUTPUT
				// WRITE: write vectors to the binary file (for MATLAB)
				ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);

				ierr = VecSet(T,t);CHKERRQ(ierr);
				ierr = VecView(T,params->viewerT);CHKERRQ(ierr);

				ierr = VecSet(ORDvec,k);CHKERRQ(ierr);
				ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);
			#endif
		}

		#ifdef DEBUG
			ierr = VecSet(ORDvec,k);CHKERRQ(ierr);
			//ierr = PetscPrintf(comm,"RESULTS: i=%d\n", i-1);CHKERRQ(ierr);
			//ierr = VecView(Y, PETSC_VIEWER_STDOUT_WORLD);
			//ierr = PetscPrintf(comm,"==========\n");CHKERRQ(ierr);
		#endif

	}// end outer while

	#ifdef DEBUG
		ierr = PetscPrintf(comm,"avg_ORD %f: %f / %d\n", (ORD/(i-1)), ORD, (i-1));CHKERRQ(ierr);
		PetscScalar maxord;
		PetscScalar minord;
		ierr = VecMax(ORDvec,NULL,&maxord);CHKERRQ(ierr);
		ierr = PetscPrintf(comm,"max_ORD: %f\n", maxord);CHKERRQ(ierr);
		ierr = VecMin(ORDvec,NULL,&minord);CHKERRQ(ierr);
		ierr = PetscPrintf(comm,"min_ORD: %f\n", minord);CHKERRQ(ierr);
	#endif

	// TIME: print only results at dt = tmax (we do not store vector T)
	// write vectors to the binary file (for MATLAB)
	// we only open bin file here and then close it immediately
	#ifdef TIME
		// STOP MEASURING TIME
		PetscTime(&end);

		ierr = PetscPrintf(PETSC_COMM_WORLD,"MTSM,N:%d,proc:%d,time:%e,steps:%d,precalc-time:0,comp-time:0,avg-ORD:%f,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n",
			params->N, size, (end-start), i, (ORD/(i-1)), data->data_size, data->info_global_sum.nz_allocated, data->info_global_sum.nz_used,data->glob_sum_nz_used_percent,data->info_global_sum.memory);CHKERRQ(ierr);

		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);

		// calculate the average ORD
		ORD = ORD/i;
		ierr = VecSet(ORDvec,ORD);CHKERRQ(ierr);
		ierr = VecView(ORDvec,params->viewerORD);CHKERRQ(ierr);

	#endif

	/////////////////////////////////////////////////////////////
	/*#ifdef LOGVIEW
		PetscPreLoadEnd();
	#endif*/

	// destroy vectors
	#ifdef OUTPUT
		ierr = VecDestroy(&T);CHKERRQ(ierr);
	#endif

	ierr = VecDestroy(&ADY);CHKERRQ(ierr);
	ierr = VecDestroy(&ORDvec);CHKERRQ(ierr);
	ierr = VecDestroy(&DYkPrev);CHKERRQ(ierr);
	ierr = VecDestroy(&Y);CHKERRQ(ierr);
	ierr = VecDestroy(&Yprev);CHKERRQ(ierr);
	ierr = VecDestroy(&maxDY);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Solves the system of ODEs numerically using explicit Taylor series method.
 * This version of the MTSM solver uses precalculated matrix A (generated by the function GenerateInputs_one_mtx())
 * Definition of the problem: y' = A*y + b
 * where A is Jacobian matrix of constants and b is a vector of
 * constants and y is a vector [U;I]
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode ExplicitTaylor_one_mtx(TParams *params, TData *data)
{
	PetscFunctionBeginUser;

	MPI_Comm       	comm;			// MPI communicator
	PetscMPIInt 	rank; 			// rank of the process
	PetscMPIInt 	size; 			// size of the group associated with a communicator
	PetscErrorCode 	ierr;

	/*****************************************
	 *  Modern Taylor Series Method (MTSM) - linear system of ODEs
	 *****************************************/
	// vectors, matrices
	Vec 			DYkPrev;    	// previous Taylor series term
	Vec    			Y;  			// solution
	Vec  			Yprev; 			// previous result of the timestep
	// ----------------------
	// default values
	PetscScalar    	ls_tol = 1e-11;     // tolerance in the last step

	// variables
	PetscScalar 	t = 0.0; 			// simulation time
	PetscScalar dt = params->dt; 		// integration step

	// indeces
	PetscInt       	i = 0;				// index for timestep

	comm = PETSC_COMM_WORLD;			// equivalent of the MPI_COMM_WORLD communicator which represents all the processs that PETSc knows about


	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
	// size of the group associated with a communicator
	ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);


	////////////////////////////////////////////////////////
	// vector DYkPrev
	ierr = VecCreate(comm,&DYkPrev);CHKERRQ(ierr);
	ierr = VecSetType(DYkPrev,VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(DYkPrev,PETSC_DECIDE,data->data_size);CHKERRQ(ierr);
	ierr = VecSet(DYkPrev,0);CHKERRQ(ierr);

	// vector Y
	ierr = VecDuplicate(DYkPrev,&Y);CHKERRQ(ierr);
	// vector Yprev
	ierr = VecDuplicate(DYkPrev,&Yprev);CHKERRQ(ierr);

	#ifdef OUTPUT
		Vec T;
		// T
		ierr = VecCreate(comm,&T);CHKERRQ(ierr);
		ierr = VecSetType(T,VECMPI);CHKERRQ(ierr);
		ierr = VecSetSizes(T,PETSC_DECIDE,1);CHKERRQ(ierr);
		ierr = VecSet(T,t);CHKERRQ(ierr);
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(T,params->viewerT);CHKERRQ(ierr);
	#endif

	////////////////////////////////////////////////////////////
	// computation
	////////////////////////////////////////////////////////////

	// START MEASURING TIME
	#ifdef TIME
		PetscLogDouble start, end;
		PetscTime(&start);
	#endif

	ierr = VecCopy(data->ic,Yprev);CHKERRQ(ierr);

	// MATLAB: y(1,:) = init';
	ierr = VecCopy(data->ic,Y);CHKERRQ(ierr);

	#ifdef OUTPUT
		// WRITE: write vector to the binary file (for MATLAB)
		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);
	#endif

	// timestep index
	i = 1;

	// MATLAB: while (t + ls_tol < tmax)
	while(t + ls_tol < params->tmax)
	{
		// MATLAB: Ay = A*y(i,:)+b;
		// v3 = v2 + A * v1
		// MatMultAdd(Mat A,Vec v1,Vec v2,Vec v3)
		ierr = MatMultAdd(data->Aone,Y,data->bone,Yprev);CHKERRQ(ierr);

		// MATLAB: y(i,:) = y(i-1,:)
		// Y = YPrev
		ierr = VecCopy(Yprev,Y);CHKERRQ(ierr);

		// shift simulation time
		t = t + dt;
		i = i + 1;

		#ifdef OUTPUT
			// WRITE: write vectors to the binary file (for MATLAB)
			ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);

			ierr = VecSet(T,t);CHKERRQ(ierr);
			ierr = VecView(T,params->viewerT);CHKERRQ(ierr);
		#endif

	}// end outer while

	// TIME: print only results at dt = tmax (we do not store vector T)
	// write vectors to the binary file (for MATLAB)
	// we only open bin file here and then close it immediately
	#ifdef TIME
		// NEW - TIME
		PetscTime(&end);
		// total_duration is the sum of the computational time using this modified MTSM and time of the generation of the matrix Aone
		PetscLogDouble total_duration;
		total_duration = data->Aone_generation_time + (end-start);

		ierr = PetscPrintf(PETSC_COMM_WORLD,"MTSM_PREC,N:%d,proc:%d,time:%e,steps:%d,precalc-time:%e,comp-time:%e,avg-ORD:%d,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n",
			params->N, size, total_duration, i, data->Aone_generation_time, (end-start), params->maxORD, data->data_size, data->info_global_sum.nz_allocated, data->info_global_sum.nz_used,data->glob_sum_nz_used_percent,data->info_global_sum.memory);CHKERRQ(ierr);

		ierr = VecView(Y,params->viewerY);CHKERRQ(ierr);
	#endif

	/////////////////////////////////////////////////////////////
 	// destroy vectors
	#ifdef OUTPUT
		ierr = VecDestroy(&T);CHKERRQ(ierr);
	#endif

	ierr = VecDestroy(&DYkPrev);CHKERRQ(ierr);
	ierr = VecDestroy(&Y);CHKERRQ(ierr);
	ierr = VecDestroy(&Yprev);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Routine for evaluating the function, where U_t = G(t,u).
 * @param ts 	TS context from TSCreate()
 * @param t 	Current time step
 * @param Y 	Input vector
 * @param F 	Function vector
 * @param ctx 	User-defined function context
 * @return Returns 0 if everything is OK, error code otherwise
 * ref: https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/TSSetRHSFunction.html
 */
PetscErrorCode RHSFunction_custom(TS ts, PetscReal t, Vec Y, Vec F, void *ctx)
{
	PetscFunctionBeginUser;
	PetscErrorCode ierr;
	AppCtx         *appctx = (AppCtx*)ctx;
	// puvodne: bez vektoru b!
	// MatMult(Mat mat,Vec x,Vec y)
	// y = Ax
	// F = A*Y
	//ierr = MatMult(appctx->A,Y,F);CHKERRQ(ierr);

	// MATLAB: dui = A*ui+b;
	// v3 = v2 + A * v1
	// MatMultAdd(Mat mat,Vec v1,Vec v2,Vec v3)
	ierr = MatMultAdd(appctx->A,Y,appctx->b,F);CHKERRQ(ierr);
	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Monitor function used at every timestep to display the iteration's progress.
 * @param ts 		TS context obtained from TSCreate()
 * @param step 		Current time step
 * @param time 		Current time
 * @param Y 		Current results
 * @param ctx 		User-defined context
 * @return Returns 0 if everything is OK, error code otherwise
 *
 * ref:
 * https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/TSMonitorSet.html
 * https://www.mcs.anl.gov/petsc/petsc-dev/src/snes/examples/tutorials/ex3.c.html
 * https://www.mcs.anl.gov/petsc/documentation/tutorials/PetscTu06.pdf
 */
PetscErrorCode MyTSMonitor(TS ts, PetscInt step, PetscReal time, Vec Y, void *ctx)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr = 0;

	AppCtx *user = (AppCtx *) ctx;

	PetscReal t_curr;
	PetscReal dt_curr;
	ierr = TSGetTime(ts,&t_curr);CHKERRQ(ierr);
	ierr = TSGetTimeStep(ts,&dt_curr);CHKERRQ(ierr);

	#ifdef OUTPUT
		ierr = VecSet(user->T,t_curr);CHKERRQ(ierr);
		// WRITE: write vector to the binary file (for MATLAB) - time
		ierr = VecView(user->T,user->viewerT);CHKERRQ(ierr);
		// WRITE: write vector to the binary file (for MATLAB) - results
		ierr = VecView(Y,user->viewerY);CHKERRQ(ierr);
	#endif

	#ifdef DEBUG
		ierr = PetscPrintf(PETSC_COMM_WORLD,"t_curr = %g, dt_curr = %g\n================\n", t_curr, dt_curr);CHKERRQ(ierr);
		//ierr = VecView(Y, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
		//ierr = PetscPrintf(PETSC_COMM_WORLD,"================\n");CHKERRQ(ierr);
	#endif

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Solves the system of ODEs numerically using explicit Runge Kutta method.
 * Definition of the problem: y' = A*y + b
 * where A is Jacobian matrix of constants and b is a vector of
 * constants and y is a vector [U;I]
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
int ExplicitRK(TParams *params, TData *data)
{
	PetscFunctionBeginUser;

	MPI_Comm       	comm;			// MPI communicator
	PetscMPIInt 	rank; 			// rank of the process
	PetscMPIInt 	size; 			// size of the group associated with a communicator
	PetscErrorCode 	ierr;

	comm = PETSC_COMM_WORLD;		// equivalent of the MPI_COMM_WORLD communicator which represents all the processs that PETSc knows about

	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
	// size of the group associated with a communicator
	ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);
	///////////////////////////////////////////////
	// TS: Scalable ODE and DAE Solvers
	// -> library TS
	// manual, page 149 (Chapter 6)

	TS             ts;
	//TSAdapt        adapt;

	AppCtx  user;
	user.A = data->A;
	user.b = data->b;

  	#ifdef OUTPUT
		Vec T;
		ierr = VecCreate(comm,&T);CHKERRQ(ierr);
		ierr = VecSetType(T,VECMPI);CHKERRQ(ierr);
		//ierr = VecSetSizes(T,1,PETSC_DECIDE);CHKERRQ(ierr);
		ierr = VecSetSizes(T,PETSC_DECIDE,1);CHKERRQ(ierr);

		// user context
		user.path =  params->dstPath;
		user.viewerY = params->viewerY;
		user.viewerT = params->viewerT;
		user.T = T;
	#endif

	#ifdef TIME
		// user context
		user.path = params->dstPath;
		user.viewerY = params->viewerY;
		user.steps = 1;

	#endif

	///////////////////////////////////////////////
	// create timestepping solver context
	ierr = TSCreate(comm,&ts);CHKERRQ(ierr);
	ierr = TSSetType(ts,TSRK);CHKERRQ(ierr);

	// RK solvers with an embedded methods:
	// TSRK3BS (ode23, default) - 3rd-order Bogacki-Shampine method with a 2nd-order embedded method
	// TSRK5DP (ode45)  - 5th-order Dormand-Prince RK scheme with a 4th-order embedded method
	// TSRK5F 			- 5th-order Fehlberg RK scheme with a 4th-order embedded method
	// TSRK5BS   		- 5th-order Bogacki-Shampine RK scheme with a 4th-order embedded method
	//
	// RK solvers without an embedded methods:
	// TSRK1FE - 1st-order, 1-stage forward Euler
	// TSRK2A - 2nd-order, 2-stage RK scheme
	// TSRK3 - 3rd-order, 3-stage RK scheme
	// TSRK4 - 4-th order, 4-stage RK scheme)

	// with embedded method
	if(params->solver == S_TSRK3BS)
	{// ode23
		ierr = TSRKSetType(ts,TSRK3BS);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK5DP)
	{// ode45
		ierr = TSRKSetType(ts,TSRK5DP);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK5F)
	{
		ierr = TSRKSetType(ts,TSRK5F);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK5BS)
	{
		ierr = TSRKSetType(ts,TSRK5BS);CHKERRQ(ierr);
	}
	// new RK solvers
	if(params->solver == S_TSRK6VR)
	{
		ierr = TSRKSetType(ts,TSRK6VR);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK7VR)
	{
		ierr = TSRKSetType(ts,TSRK7VR);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK8VR)
	{
		ierr = TSRKSetType(ts,TSRK8VR);CHKERRQ(ierr);
	}

	// without embedded method
	if(params->solver == S_TSRK1FE)
	{
		ierr = TSRKSetType(ts,TSRK1FE);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK2A)
	{
		ierr = TSRKSetType(ts,TSRK2A);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK3)
	{
		ierr = TSRKSetType(ts,TSRK3);CHKERRQ(ierr);
	}
	if(params->solver == S_TSRK4)
	{
		ierr = TSRKSetType(ts,TSRK4);CHKERRQ(ierr);
	}

	// special case: manual, page 156, section 6.5: u' = Au
	ierr = TSSetProblemType(ts,TS_LINEAR);CHKERRQ(ierr);

	// Sets the routine for evaluating the function, where U_t = G(t,u)
	ierr = TSSetRHSFunction(ts,NULL,RHSFunction_custom,&user);CHKERRQ(ierr);
	ierr = TSSetRHSJacobian(ts,data->A,data->A,TSComputeRHSJacobianConstant,NULL);CHKERRQ(ierr);

	// initial conditions
	ierr = TSSetSolution(ts, data->ic);CHKERRQ(ierr);

	// set tmax and final time-stepping approach
	ierr = TSSetMaxTime(ts,params->tmax);CHKERRQ(ierr);
	ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_MATCHSTEP);CHKERRQ(ierr);
	//ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);

	// set absolute and relative tolerances
	ierr = TSSetTolerances(ts,params->eps,NULL,params->eps,NULL);CHKERRQ(ierr);

	// set initial t=0
	ierr = TSSetTime(ts,0.0);CHKERRQ(ierr);

	// set integration step
	ierr = TSSetTimeStep(ts,params->dt);CHKERRQ(ierr);

	// monitor the solution
	#ifdef OUTPUT
		ierr = TSMonitorSet(ts,MyTSMonitor,&user,NULL);CHKERRQ(ierr);
	#endif

	// monitor the solution
	#ifdef DEBUG
		ierr = TSMonitorSet(ts,MyTSMonitor,&user,NULL);CHKERRQ(ierr);
	#endif

	/////////////////////////////////////
	// START MEASURING TIME
	#ifdef TIME
		PetscLogDouble startRK, endRK;
		PetscTime(&startRK);
	#endif

	// solve the linear system of ODEs
	ierr = TSSolve(ts,data->ic);CHKERRQ(ierr);


	#ifdef TIME
		PetscInt       steps;
		//PetscReal      tmax = 0.0;
		ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);

		// STOP MEASURING TIME
		PetscTime(&endRK);
		PetscPrintf(PETSC_COMM_WORLD,"%s,N:%d,proc:%d,time:%e,steps:%d,precalc-time:0,comp-time:0,avg-ORD:0,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n",
			SOLVER_STR[params->solver],  params->N, size, (endRK-startRK), steps, data->data_size, data->info_global_sum.nz_allocated, data->info_global_sum.nz_used,data->glob_sum_nz_used_percent,data->info_global_sum.memory);CHKERRQ(ierr);

		ierr = VecView(data->ic,user.viewerY);CHKERRQ(ierr);
	#endif

	#ifdef OUTPUT
		PetscInt       steps;
	  	PetscReal      tmax = 0.0;
		ierr = TSGetSolveTime(ts,&tmax);CHKERRQ(ierr);
		ierr = TSGetStepNumber(ts,&steps);CHKERRQ(ierr);
		ierr = PetscPrintf(comm, "%s - numStep: %d, tmax: %g\n", SOLVER_STR[params->solver], steps, tmax);CHKERRQ(ierr);
	#endif

	ierr = TSDestroy(&ts);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Solves the system of ODEs and call appropriate solver (version of MTSM or RK)
 * @param params 				Parameters for the calculation
 * @param data 					Input data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode Solve(TParams *params, TData* data)
{
	PetscFunctionBeginUser;
	PetscErrorCode ierr;

	PetscMPIInt    	size;  // size of the group associated with a communicator

	// size of the group associated with a communicator
	ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);

	#ifdef LOGVIEW
		PetscPreLoadBegin(PETSC_FALSE, "Starting");
	#endif

  	if(params->solverType == ST_TAYLOR)
	{// MTSM - set MAXORD

		if(params->solver == S_MTSM)
		{
			#ifdef LOGVIEW
				/*PetscLogStage  stage_MTSM;
				PetscLogStageRegister("MTSM", &stage_MTSM);
				PetscLogStagePush(stage_MTSM);*/

				PetscPreLoadStage("MTSM");
			#endif

			ierr = ExplicitTaylor_o1(params, data);CHKERRQ(ierr);

			/*#ifdef LOGVIEW
				PetscLogStagePop();
			#endif*/
		}
		else if(params->solver == S_MTSM_O2)
		{
			#ifdef LOGVIEW
				/*PetscLogStage  stage_MTSM;
				PetscLogStageRegister("MTSM", &stage_MTSM);
				PetscLogStagePush(stage_MTSM);*/

				PetscPreLoadStage("MTSM_O2");
			#endif

			ierr = ExplicitTaylor_o2(params, data);CHKERRQ(ierr);
		}
		else if(params->solver == S_MTSM_PRECALC)
		{
			#ifdef LOGVIEW
				/*PetscLogStage  stage_MTSM_PREC[2];
				PetscLogStageRegister("MTSM_PREC_GEN", &stage_MTSM_PREC[0]);
				PetscLogStageRegister("MTSM_PREC_CALC", &stage_MTSM_PREC[1]);

				PetscLogStagePush(stage_MTSM_PREC[0]);*/

				PetscPreLoadStage("MTSM_PREC_GEN");
			#endif

			/*#ifdef LOGVIEW
				PetscLogStagePop();
			#endif*/

			#ifdef LOGVIEW
				/*PetscLogStage  stage_MTSM_PREC;
				PetscLogStageRegister("MTSM_PREC", &stage_MTSM_PREC);
				PetscLogStagePush(stage_MTSM_PREC[1]);*/

				PetscPreLoadStage("MTSM_PREC_CALC");
			#endif
			// MTSM WITH PRECALCULATION
			ierr = ExplicitTaylor_one_mtx(params, data);CHKERRQ(ierr);

			/*#ifdef LOGVIEW
				PetscLogStagePop();
			#endif*/
		}
	}
	else if(params->solverType == ST_RK)
	{
		#ifdef LOGVIEW
			/*PetscLogStage  stage_RK;
			//PetscLogStageRegister("RK", &stage_RK);
			PetscLogStageRegister(SOLVER_STR[params->solver], &stage_RK);
			PetscLogStagePush(stage_RK);*/

			PetscPreLoadStage("RK");
		#endif

		// RK methods
		ierr = ExplicitRK(params, data);CHKERRQ(ierr);

		/*#ifdef LOGVIEW
			PetscLogStagePop();
		#endif*/
	}

	// compare with a nalytical solution
	#ifdef LOGVIEW
		PetscPreLoadEnd();
	#endif

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Main program.
 */
int main(int argc,char **args)
{

	PetscErrorCode 	ierr;			// error code
	//PetscViewer    	viewer; 		// viewer (for PETSc objects)
	MPI_Comm       	comm;			// MPI communicator
	PetscMPIInt 	rank; 			// rank of the process
	PetscMPIInt 	size; 			// size of the group associated with a communicator

	PetscInt rows, cols; 			// number of rows and cols of input matrix A

	TParamsTelegraph paramsTelegraph;
	TParams params;					// computational parameters
	TData data;						// input data

	// init PETSc
	ierr = PetscInitialize(&argc,&args,NULL,NULL);
	if (ierr) return ierr;

	//viewer = PETSC_VIEWER_STDOUT_WORLD; //PETSC_VIEWER_ASCII_MATLAB;//PETSC_VIEWER_STDOUT_WORLD;//PETSC_VIEWER_STDOUT_WORLD;
	comm = PETSC_COMM_WORLD;

	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
	// size of the group associated with a communicator
	ierr = MPI_Comm_size(comm,&size);CHKERRQ(ierr);

	// get general parameters
	ierr = GetParams(&params);CHKERRQ(ierr);


	#ifdef DEBUG
		ierr = PrintParams(&params);CHKERRQ(ierr);

		if(params.problemType == PT_TELEGRAPH_SIMPLE || params.problemType == PT_TELEGRAPH_FULL)
		{
			ierr = PrintParamsTelegraph(&paramsTelegraph);CHKERRQ(ierr);
		}
	#endif


	// telegraph line
	if(params.problemType == PT_TELEGRAPH_SIMPLE || params.problemType == PT_TELEGRAPH_FULL)
	{
		// telegraph line - we need to have these parameters, because dt, tmax depends on the N, L, C parameters!
		// we use default parameters (SetDefaultPramasTelegraph()), adjusted line (R1=100, R2=100)
		ierr = GetParamsTelegraph(&params, &paramsTelegraph);CHKERRQ(ierr);

		//ierr = PrintParams(&params);CHKERRQ(ierr);
		//ierr = PrintParamsTelegraph(&paramsTelegraph);CHKERRQ(ierr);
	}

	////////////////////////////////////////////////////////////
	if(params.mode == SOLVE)
	{
		// load the input data
		ierr = LoadInputData(&params, &data);CHKERRQ(ierr);

		#ifdef DEBUG
			ierr = PrintMatrix(&(data.A), "main: mat A\n");
			ierr = PrintParams(&params);CHKERRQ(ierr);
		#endif

		/*ierr = PrintMatrixOwnership(&(data.A), "main: A matrix ownership");CHKERRQ(ierr);
		PetscInt rows, cols;
		ierr = MatGetLocalSize(data.A, &rows, &cols);CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,"main: local size: %d, %d\n", rows, cols);CHKERRQ(ierr);
		ierr = PetscPrintf(PETSC_COMM_WORLD,"====================\n");CHKERRQ(ierr);*/


		// precalculate matrix A
		// NOTE: for profiling, we should add PetscLogStageRegister() here
		if(params.solver == S_MTSM_PRECALC)
		{
			//ierr = PetscPrintf(PETSC_COMM_WORLD,"main: GenerateInputs_one_mtx\n");CHKERRQ(ierr);
			//ierr = GenerateInputs_one_mtx(&params, &data);CHKERRQ(ierr);
			//ierr = PetscPrintf(PETSC_COMM_WORLD,"main: GenerateInputs_one_mtx_ALL\n");CHKERRQ(ierr);
			ierr = GenerateInputs_one_mtx_all(&params, &data);CHKERRQ(ierr);

			// WARNING: View Aone matrix in the MATLAB format!!!

			/*PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_MATLAB);
			MatView(data.Aone,PETSC_VIEWER_STDOUT_WORLD);
			PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);*/

			// old: ierr = PrintMatrix(&(data.Aone), " PRECALC");CHKERRQ(ierr);

			// get matrix contet information and also calculate number of nonzeroes
			ierr = GetMatrixContext(&(data.Aone), &(data.info_global_sum), &(data.glob_sum_nz_used_percent), MAT_GLOBAL_SUM);CHKERRQ(ierr);
		}
		else
		{
			// get matrix contet information and also calculate number of nonzeroes
			ierr = GetMatrixContext(&(data.A), &(data.info_global_sum), &(data.glob_sum_nz_used_percent), MAT_GLOBAL_SUM);CHKERRQ(ierr);
		}


		#ifdef DEBUG
			// print matrix context information
			if(params.solver == S_MTSM_PRECALC)
			{
				ierr = PrintMatrixContext(&(data.info_global_sum), &(data.glob_sum_nz_used_percent), MAT_GLOBAL_SUM,  "main: data.Aone");CHKERRQ(ierr);
			}
			else
			{
				ierr = PrintMatrixContext(&(data.info_global_sum), &(data.glob_sum_nz_used_percent), MAT_GLOBAL_SUM,  "main: data.A");CHKERRQ(ierr);
			}
		#endif

		// get the real data size
		// Note: matrix A and Aone have always the same size,
		// therefore, it does not matter from which one we get the size
		ierr = MatGetSize(data.A, &rows, &cols);CHKERRQ(ierr);
		data.data_size = rows;

		// set PETSc viewers (opens binary files)
		ierr = SetViewers(&params);CHKERRQ(ierr);
		// NOTE: zobrazeni udaje o matici
		// - zakomentovat Solve()
		// odkomentovat PEtscPrintf()
		// solve the problem
		ierr = Solve(&params, &data);CHKERRQ(ierr);
		//ierr = PetscPrintf(PETSC_COMM_WORLD,"MTSM_PREC,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n",
		//	data.info_global_sum.nz_allocated, data.info_global_sum.nz_used, data.glob_sum_nz_used_percent, data.info_global_sum.memory);CHKERRQ(ierr);

		// free memory
		ierr = DestroyData(&params, &data);CHKERRQ(ierr);

		// free viewers
		ierr = DestroyViwers(&params);CHKERRQ(ierr);

	}
	else if(params.mode == PRINT)
	{
		ierr = PrintParams(&params);CHKERRQ(ierr);

		if(params.problemType == PT_TELEGRAPH_SIMPLE || params.problemType == PT_TELEGRAPH_FULL)
		{
			ierr = PrintParamsTelegraph(&paramsTelegraph);CHKERRQ(ierr);
		}
	}
	else
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"Unsupported program mode! Supported modes are solve or print!\n");CHKERRQ(ierr);
		return -1;
	}

	ierr = PetscFinalize();
	return 0;
}
