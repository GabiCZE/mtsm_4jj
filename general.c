/**
 * Project: Parallel solution of PDEs using Method of Lines (MOL)
 * The MOL discretizes one dimension and then integrates the semi-discrete problem as a system of ODEs.
 * The resulting system of ODEs can be solved using standard numerical methods - MTSM and RK solvers.
 * File: general.c
 * Author: Gabriela Necasova, inecasova@fit.vutbr.cz
 * Date: August 2018
 * Modification: November 2020
 */

#include <petscksp.h>
#include "general.h"

// ======================================================================
/**
 * Generates input matrix A for given maxORD, vectors ic (initial conditions) and vector b.
 * ONLY matrix A is precalculated for all Taylor series terms [(h^i/i!) * A^i] for i=1..maxORD in one matrix
 * -----------------------------------------------------------
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode GenerateInputs_one_mtx(TParams *params, TData *data)
{

	PetscFunctionBeginUser;

	MPI_Comm       	comm;
	PetscErrorCode 	ierr;
	comm = PETSC_COMM_WORLD;

	// vectors, matrices
	Mat Ione;
	Mat Adt;
	Mat C;
	Mat Ynominator;
	Vec diagVec;
	// -----------------

	PetscScalar denominator = 1.0;
	PetscInt i;

	PetscInt data_size;
	PetscInt rows;
	PetscInt cols;

	ierr = MatGetSize(data->A, &rows, &cols);
	data_size = rows;

	/*****************************************
	 *  create matrix Aone
	 *****************************************/
	ierr = MatCreate(comm, &(data->Aone));CHKERRQ(ierr);
	ierr = MatSetType(data->Aone, MATMPIAIJ);CHKERRQ(ierr);
	ierr = MatSetSizes(data->Aone, PETSC_DECIDE,PETSC_DECIDE, data_size, data_size);CHKERRQ(ierr);

	// if preallocation is not used, we have to call MatSetUp()
	// sets up the internal matrix data structures for the later use
	//ierr = MatSetUp(A);CHKERRQ(ierr);

	// parallel sparse - preallocation for better performance
	// TD: Pokud se matice predpocita, tak je dense, takze prealokace ma byt vlastne na celou velikost matice?
	// TD: protoze predtim jsem to pocitala s puvodni sparse matici, proto tam byla hodnota 4
	// TD: vice info v GDoc: https://docs.google.com/document/d/1jeFjaP72CQO1nR7kpy2uVFrRhDPsF7KYS4ByH0ejt1E/edit?usp=sharing
	// TD: Tedy:
	// TD: ierr = MatMPIAIJSetPreallocation(Aone,4,PETSC_NULL,4,PETSC_NULL);CHKERRQ(ierr);
	ierr = MatMPIAIJSetPreallocation(data->Aone, data_size, PETSC_NULL, data_size, PETSC_NULL);CHKERRQ(ierr);

	ierr = MatAssemblyBegin(data->Aone, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(data->Aone, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	/*****************************************
	 *  create matrix I (diagonal matrix)
	 *****************************************/
	ierr = MatCreate(comm, &Ione);CHKERRQ(ierr);
	ierr = MatSetType(Ione, MATMPIAIJ);CHKERRQ(ierr);
	ierr = MatSetSizes(Ione, PETSC_DECIDE, PETSC_DECIDE, data_size, data_size);CHKERRQ(ierr);

	// if preallocation is not used, we have to call MatSetUp()
	// sets up the internal matrix data structures for the later use
	//ierr = MatSetUp(A);CHKERRQ(ierr);

	// parallel sparse - preallocation for better performance
	ierr = MatMPIAIJSetPreallocation(Ione, 1, PETSC_NULL, 0, PETSC_NULL);CHKERRQ(ierr);

	// vector with ones
	ierr = VecCreate(PETSC_COMM_WORLD, &diagVec);CHKERRQ(ierr);
	ierr = VecSetType(diagVec, VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(diagVec, PETSC_DECIDE, data_size);CHKERRQ(ierr);
	ierr = VecSet(diagVec, 1);CHKERRQ(ierr);
	// set the diagonal of matrix I using the vector diagVec
	ierr = MatDiagonalSet(Ione, diagVec, INSERT_VALUES);CHKERRQ(ierr);

	ierr = MatAssemblyBegin(Ione, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(Ione, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	/*****************************************
	 *  create matrix Adt
	 *****************************************/
	// copy matrix A to Adt
	// NOTE: we use MatDuplicate() because it CREATES a new matrix,
	// while MatCopy() copies values from
	// an already existing matrix to another already existing matrix.
	ierr =  MatDuplicate(data->A, MAT_COPY_VALUES, &Adt);CHKERRQ(ierr);

	// scale all elements of a matrix by integration step dt
	ierr = MatScale(Adt, params->dt);CHKERRQ(ierr);

	/*****************************************
	 *  create matrix Ynominator
	 *****************************************/
	ierr =  MatDuplicate(Adt, MAT_COPY_VALUES, &Ynominator);CHKERRQ(ierr);

	#ifdef TIME
		PetscLogDouble start, end;
		PetscTime(&start);
	#endif

	/*****************************************
	 *  main cycle
	 *****************************************/
	for(i = 1; i <= params->maxORD; i++)
	{
		// A_one = A_one + Ynominator/denominator;
		ierr = MatAXPY(data->Aone, (1/denominator), Ynominator, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

		// Ynominator = Ynominator*(A*dt);
		// PETSc: C = Ynominator * Adt;
		ierr = MatMatMult(Ynominator, Adt, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);CHKERRQ(ierr);
		// PETSc: Ynominator = C;
		//ierr = MatCopy(C, Ynominator, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
		ierr = MatDuplicate(C, MAT_COPY_VALUES, &Ynominator);CHKERRQ(ierr);

		// TODO: destroy matrix C to avoid high memory consumption?
		// probably, matrix C is allocated in each cycle and new pointer to the created matrix exists
		// so we should duplicate values from C to Ynominator and then destroy
		// We are already destroying the matrix C at the end of this function, but I am afraid, that it only destroys the LATEST created matrix C
		//ierr = MatDestroy(&C);CHKERRQ(ierr);

		denominator = (i+1)*denominator;
	}

	// A_one = A_one + I;
	ierr = MatAXPY(data->Aone, 1.0, Ione, SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);

	#ifdef TIME
		PetscTime(&end);
		data->Aone_generation_time = (end-start);
	#endif


	ierr = VecDestroy(&diagVec);CHKERRQ(ierr);

	ierr = MatDestroy(&Ione);CHKERRQ(ierr);
	ierr = MatDestroy(&Adt);CHKERRQ(ierr);
	ierr = MatDestroy(&Ynominator);CHKERRQ(ierr);
	ierr = MatDestroy(&C);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Generates input matrix A for given maxORD, vectors ic (initial conditions) and vector b.
 * matrix A is precalculated for all Taylor series terms [(h^i/i!) * A^i] for i=1..maxORD in one matrix
 * NOTE: precalculate both - matrix A and vector b
 * -----------------------------------------------------------
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode GenerateInputs_one_mtx_all(TParams *params, TData *data)
{

	PetscFunctionBeginUser;

	MPI_Comm       	comm;
	PetscErrorCode 	ierr;
	comm = PETSC_COMM_WORLD;

	// vectors, matrices
	Mat Ione;
	Mat Adt;
	Mat C;
	Mat Ynominator;
	Vec diagVec;
	Vec Bnominator;
	// -----------------

	PetscScalar denominator = 1.0;
	PetscInt i;

	PetscInt data_size;
	PetscInt rows;
	PetscInt cols;

	ierr = MatGetSize(data->A, &rows, &cols);
	data_size = rows;

	/*****************************************
	 *  create matrix Aone
	 *****************************************/
	ierr = MatCreate(comm, &(data->Aone));CHKERRQ(ierr);
	ierr = MatSetType(data->Aone, MATMPIAIJ);CHKERRQ(ierr);
	//ierr = MatSetType(data->Aone, MATMPIDENSE);CHKERRQ(ierr);
	ierr = MatSetSizes(data->Aone, PETSC_DECIDE,PETSC_DECIDE, data_size, data_size);CHKERRQ(ierr);

	// if preallocation is not used, we have to call MatSetUp()
	// sets up the internal matrix data structures for the later use
	//ierr = MatSetUp(A);CHKERRQ(ierr);

	// parallel sparse - preallocation for better performance
	// TD: Pokud se matice predpocita, tak je dense, takze prealokace ma byt vlastne na celou velikost matice?
	// TD: protoze predtim jsem to pocitala s puvodni sparse matici, proto tam byla hodnota 4
	// TD: vice info v GDoc: https://docs.google.com/document/d/1jeFjaP72CQO1nR7kpy2uVFrRhDPsF7KYS4ByH0ejt1E/edit?usp=sharing
	// TD: Tedy:
	// TD: ierr = MatMPIAIJSetPreallocation(Aone,4,PETSC_NULL,4,PETSC_NULL);CHKERRQ(ierr);
	//ierr = MatMPIAIJSetPreallocation(data->Aone, data_size, PETSC_NULL, data_size, PETSC_NULL);CHKERRQ(ierr);
	if(params->problemType == PT_TELEGRAPH_SIMPLE || params->problemType == PT_TELEGRAPH_FULL)
	{
		ierr = MatMPIAIJSetPreallocation(data->Aone, 65, PETSC_NULL, 65, PETSC_NULL);CHKERRQ(ierr);
	}
	else
	{
		ierr = MatMPIAIJSetPreallocation(data->Aone, data_size, PETSC_NULL, data_size, PETSC_NULL);CHKERRQ(ierr);
	}


	ierr = MatAssemblyBegin(data->Aone, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
	ierr = MatAssemblyEnd(data->Aone, MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	/*****************************************
	 *  create matrix I (diagonal matrix)
	 *****************************************/
	ierr = MatCreate(comm, &Ione);CHKERRQ(ierr);
	ierr = MatSetType(Ione, MATMPIAIJ);CHKERRQ(ierr);
	ierr = MatSetSizes(Ione, PETSC_DECIDE, PETSC_DECIDE, data_size, data_size);CHKERRQ(ierr);

	// if preallocation is not used, we have to call MatSetUp()
	// sets up the internal matrix data structures for the later use
	//ierr = MatSetUp(A);CHKERRQ(ierr);

	// parallel sparse - preallocation for better performance
	//ierr = MatMPIAIJSetPreallocation(A,N*2+2,PETSC_NULL,N*2+2,PETSC_NULL);CHKERRQ(ierr);
	ierr = MatMPIAIJSetPreallocation(Ione, 1, PETSC_NULL, 0, PETSC_NULL);CHKERRQ(ierr);

	// vector with ones
	ierr = VecCreate(PETSC_COMM_WORLD, &diagVec);CHKERRQ(ierr);
	ierr = VecSetType(diagVec, VECMPI);CHKERRQ(ierr);
	ierr = VecSetSizes(diagVec, PETSC_DECIDE, data_size);CHKERRQ(ierr);
	ierr = VecSet(diagVec, 1);CHKERRQ(ierr);
	// set the diagonal of matrix I using the vector diagVec
	ierr = MatDiagonalSet(Ione, diagVec, INSERT_VALUES);CHKERRQ(ierr);

	ierr = MatAssemblyBegin(Ione, MAT_FINAL_ASSEMBLY);
	ierr = MatAssemblyEnd(Ione, MAT_FINAL_ASSEMBLY);


	/*****************************************
	 *  create matrix Adt
	 *****************************************/
	// copy matrix A to Adt
	// Adt = A
	ierr =  MatDuplicate(data->A, MAT_COPY_VALUES, &Adt);CHKERRQ(ierr);

	// scale all elements of a matrix by integration step dt
	// Adt = Adt * dt
	ierr = MatScale(Adt, params->dt);CHKERRQ(ierr);

	/*****************************************
	 *  create matrix Ynominator
	 *****************************************/
	// Ynominator = Adt
	ierr =  MatDuplicate(Adt, MAT_COPY_VALUES, &Ynominator);CHKERRQ(ierr);

	/*****************************************
	 *  create vector Bnominator
	 *****************************************/
	// Bnominator = b*dt;
	ierr = VecDuplicate(data->b,&(data->bone));CHKERRQ(ierr);
	ierr = VecDuplicate(data->bone,&Bnominator);CHKERRQ(ierr);
	ierr = VecScale(Bnominator,params->dt);CHKERRQ(ierr);

	#ifdef TIME
		PetscLogDouble start, end;
		PetscTime(&start);
	#endif

	/*****************************************
	 *  main cycle
	 *****************************************/
	for(i = 1; i <= params->maxORD; i++)
	{
		// PetscErrorCode MatAXPY(Mat Y,PetscScalar a,Mat X,MatStructure str)
		// Y = a*X + Y

		// A_one = A_one + Ynominator/denominator;
		ierr = MatAXPY(data->Aone, (1/denominator), Ynominator, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);

		// b_one = b_one + Bnominator/denominator;
		ierr = VecAXPY(data->bone, (1/denominator), Bnominator);CHKERRQ(ierr);

		// Ynominator = Ynominator*(A*dt);
		ierr = MatMatMult(Ynominator, Adt, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &C);CHKERRQ(ierr);
		// ierr = MatCopy(C, Ynominator, DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
		ierr = MatDuplicate(C, MAT_COPY_VALUES, &Ynominator);CHKERRQ(ierr);

		// Bnominator = (A*dt)*Bnominator;
		// y = Ax

		// PETSc: diagVec = (A*dt)*Bnominator;
		ierr = MatMult(Adt, Bnominator, diagVec);CHKERRQ(ierr);
		// PETSc: Bnominator = diagVec;
		ierr = VecDuplicate(diagVec,&Bnominator);CHKERRQ(ierr);

		denominator = (i+1)*denominator;
	}

	// A_one = A_one + I;
	ierr = MatAXPY(data->Aone, 1.0, Ione, SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);

	#ifdef TIME
		PetscTime(&end);
		data->Aone_generation_time = (end-start);
	#endif

	#ifdef DEBUG
		MatInfo info;
		PetscScalar nz_used_percent;

		ierr = GetMatrixContext(&Ione,&info, &nz_used_percent,MAT_GLOBAL_SUM);CHKERRQ(ierr);
		ierr = PrintMatrixContext(&info, &nz_used_percent,MAT_GLOBAL_SUM, "GenerateInputs_one_mtx_all: Ione");CHKERRQ(ierr);

		ierr = GetMatrixContext(&Adt,&info, &nz_used_percent,MAT_GLOBAL_SUM);CHKERRQ(ierr);
		ierr = PrintMatrixContext(&info, &nz_used_percent,MAT_GLOBAL_SUM, "GenerateInputs_one_mtx_all: Adt");CHKERRQ(ierr);

		ierr = GetMatrixContext(&C,&info, &nz_used_percent,MAT_GLOBAL_SUM);CHKERRQ(ierr);
		ierr = PrintMatrixContext(&info, &nz_used_percent,MAT_GLOBAL_SUM, "GenerateInputs_one_mtx_all: C");CHKERRQ(ierr);

		ierr = GetMatrixContext(&Ynominator,&info, &nz_used_percent,MAT_GLOBAL_SUM);CHKERRQ(ierr);
		ierr = PrintMatrixContext(&info, &nz_used_percent,MAT_GLOBAL_SUM, "GenerateInputs_one_mtx_all: Ynominator");CHKERRQ(ierr);

		//ierr = PrintMatrix(&data->Aone, " PRECALC");CHKERRQ(ierr);
		/*PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_MATLAB);
		MatView(data->A,PETSC_VIEWER_STDOUT_WORLD);
		PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);*/

	#endif

	ierr = VecDestroy(&diagVec);CHKERRQ(ierr);
	ierr = VecDestroy(&Bnominator);CHKERRQ(ierr);

	ierr = MatDestroy(&Ione);CHKERRQ(ierr);
	ierr = MatDestroy(&Adt);CHKERRQ(ierr);
	ierr = MatDestroy(&Ynominator);CHKERRQ(ierr);
	ierr = MatDestroy(&C);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Gets command line parameters (general).
 * @param params 	Parameters for the calculation
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode GetParams(TParams *params)
{

	PetscFunctionBeginUser;

	PetscErrorCode ierr;
	PetscBool flg;

	char tmp[PETSC_MAX_PATH_LEN] = "";

	// size of the problem
	ierr = PetscOptionsGetInt(NULL,NULL,"-N",&params->N,NULL);CHKERRQ(ierr);

	// precision
	ierr = PetscOptionsGetScalar(NULL,NULL,"-eps",&params->eps,NULL);CHKERRQ(ierr);

	// precision - RK solvers
	ierr = PetscOptionsGetScalar(NULL,NULL,"-eps_rk",&params->eps_rk,NULL);CHKERRQ(ierr);

	// integration step
	ierr = PetscOptionsGetScalar(NULL,NULL,"-dt",&params->dt,NULL);CHKERRQ(ierr);

	// maximum simulation time
	ierr = PetscOptionsGetScalar(NULL,NULL,"-tmax",&params->tmax,NULL);CHKERRQ(ierr);


	// problem type
	ierr = PetscOptionsGetString(NULL,NULL,"-pType",tmp,sizeof(tmp),&flg);CHKERRQ(ierr);
	if(strcmp(tmp, PROBLEM_TYPE_STR[PT_TELEGRAPH_SIMPLE]) == 0)
	{
		params->problemType = PT_TELEGRAPH_SIMPLE;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_TELEGRAPH_FULL]) == 0)
	{
		params->problemType = PT_TELEGRAPH_FULL;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_STOKES]) == 0)
	{
		params->problemType = PT_STOKES;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_WAVE_FSFO_combined]) == 0)
	{
		params->problemType = PT_WAVE_FSFO_combined;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_LAPLACE_2D]) == 0)
	{
		params->problemType = PT_LAPLACE_2D;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_HEAT2D_SIN]) == 0)
	{
		params->problemType = PT_HEAT2D_SIN;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_HEAT1D_SIN]) == 0)
	{
		params->problemType = PT_HEAT1D_SIN;
	}
	else if(strcmp(tmp, PROBLEM_TYPE_STR[PT_SINCOS]) == 0)
	{
		params->problemType = PT_SINCOS;
	}
	else
	{
		PetscPrintf(PETSC_COMM_WORLD, "Unknown problemType!\n");
		return -1;
	}
	// TODO: conditions for other types of Wave Equation FD

	// maximal order of the MTSM
	ierr = PetscOptionsGetInt(NULL,NULL,"-maxORD",&params->maxORD,NULL);CHKERRQ(ierr);

	// solver type
	ierr = PetscOptionsGetString(NULL,NULL,"-solType",tmp,sizeof(tmp),&flg);CHKERRQ(ierr);
	if(strcmp(tmp, SOLVER_TYPE_STR[ST_TAYLOR]) == 0)
	{
		params->solverType = ST_TAYLOR;
	}
	else if(strcmp(tmp, SOLVER_TYPE_STR[ST_RK]) == 0)
	{
		params->solverType = ST_RK;
	}
	else
	{
		PetscPrintf(PETSC_COMM_WORLD, "Unknown solverType!\n");
		return -1;
	}

	// solver method
	ierr = PetscOptionsGetString(NULL,NULL,"-solver",tmp,sizeof(tmp),&flg);CHKERRQ(ierr);

	// MTSM without precalculation
	if(strcmp(tmp, SOLVER_STR[S_MTSM]) == 0)
	{
		params->solver = S_MTSM;
	}
	// MTSM without precalculation, optimization
	else if(strcmp(tmp, SOLVER_STR[S_MTSM_O2]) == 0)
	{
		params->solver = S_MTSM_O2;
	}
	// MTSM with precalculation
	else if(strcmp(tmp, SOLVER_STR[S_MTSM_PRECALC]) == 0)
	{
		params->solver = S_MTSM_PRECALC;
	}

	// with embedded method
	else if(strcmp(tmp, SOLVER_STR[S_TSRK3BS]) == 0)
	{// ode23
		params->solver = S_TSRK3BS;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK5DP]) == 0)
	{// ode45
		params->solver = S_TSRK5DP;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK5F]) == 0)
	{
		params->solver = S_TSRK5F;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK5BS]) == 0)
	{
		params->solver = S_TSRK5BS;
	}


	// new solvers
	else if(strcmp(tmp, SOLVER_STR[S_TSRK6VR]) == 0)
	{
		params->solver = S_TSRK6VR;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK7VR]) == 0)
	{
		params->solver = S_TSRK7VR;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK8VR]) == 0)
	{
		params->solver = S_TSRK8VR;
	}

	// without embedded method
	else if(strcmp(tmp, SOLVER_STR[S_TSRK1FE]) == 0)
	{
		params->solver = S_TSRK1FE;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK2A]) == 0)
	{
		params->solver = S_TSRK2A;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK3]) == 0)
	{
		params->solver = S_TSRK3;
	}
	else if(strcmp(tmp, SOLVER_STR[S_TSRK4]) == 0)
	{
		params->solver = S_TSRK4;
	}
	else
	{
		PetscPrintf(PETSC_COMM_WORLD, "Unknown solver!\n");
		return -1;
	}

	// src path
	ierr = PetscOptionsGetString(NULL,NULL,"-srcPath",params->srcPath,sizeof(params->srcPath),&flg);CHKERRQ(ierr);

	// dst path
	ierr = PetscOptionsGetString(NULL,NULL,"-dstPath",params->dstPath,sizeof(params->dstPath),&flg);CHKERRQ(ierr);

	// program mode
	ierr = PetscOptionsGetString(NULL,NULL,"-mode",tmp,sizeof(tmp),&flg);CHKERRQ(ierr);
	if(strcmp(tmp, MODE_STR[SOLVE]) == 0)
	{
		params->mode = SOLVE;
	}
	else if(strcmp(tmp, MODE_STR[PRINT]) == 0)
	{
		params->mode = PRINT;
	}
	else if(strcmp(tmp, "debug") == 0)
	{
		params->mode = SOLVE;
	}
	else
	{
		PetscPrintf(PETSC_COMM_WORLD, "Unknown mode!\n");
		return -1;
	}

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints default parameters for calculation.
 * @param params 	Parameters for the calculation
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintParams(TParams *params)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr;

	PetscMPIInt 	size; 			// size of the group associated with a communicator

	// size of the group associated with a communicator
	ierr = MPI_Comm_size(PETSC_COMM_WORLD,&size);CHKERRQ(ierr);

	ierr = PetscPrintf(PETSC_COMM_WORLD,"np = %d\n", size);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"N = %d\n", params->N);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"eps = %g\n", params->eps);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"eps_rk = %g\n", params->eps_rk);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"dt = %g\n", params->dt);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"tmax = %g\n", params->tmax);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"maxORD = %d\n", params->maxORD);CHKERRQ(ierr);

	ierr = PetscPrintf(PETSC_COMM_WORLD,"problemType = %s\n", PROBLEM_TYPE_STR[params->problemType]);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"solverType = %s\n", SOLVER_TYPE_STR[params->solverType]);CHKERRQ(ierr);

	ierr = PetscPrintf(PETSC_COMM_WORLD,"solver = %s\n", SOLVER_STR[params->solver]);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"srcPath = %s\n", params->srcPath);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"dstPath = %s\n\n", params->dstPath);CHKERRQ(ierr);

	//ierr = PetscPrintf(PETSC_COMM_WORLD,"mode = %s\n", MODE_STR[params->mode]);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);

}

// ======================================================================
/**
 * Prints default parameters for the Telegraph equation.
 * @param paramsTelegraph 	Parameters for the Telegraph equation
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintParamsTelegraph(TParamsTelegraph *paramsTelegraph)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr;
	ierr = PetscPrintf(PETSC_COMM_WORLD,"R1 = %g\n", paramsTelegraph->R1);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"R2 = %g\n", paramsTelegraph->R2);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"L = %g\n", paramsTelegraph->L);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"C = %g\n", paramsTelegraph->C);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"om = %g\n", paramsTelegraph->om);CHKERRQ(ierr);

	// note: data are printed in the solveTelegraph() function
	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Sets default parameters for the Telegraph equation.
 * @param paramsTelegraph 	Parameters for the Telegraph equation
 * @return Returns 0 if everything is OK, error code otherwise
 */
void SetDefaultParamsTelegraph(TParamsTelegraph *paramsTelegraph)
{
	paramsTelegraph->R1 = 100.0;
	paramsTelegraph->R2 = 100.0;
	paramsTelegraph->L = 1e-8;
	paramsTelegraph->C = 1e-12;
	paramsTelegraph->om = 3e9;
}

// ======================================================================
 /**
 * Gets default parameters for the Telegraph equation.
 * @param params 				Parameters for the calculation
 * @param paramsTelegraph 		Parameters for the Telegraph equation
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode GetParamsTelegraph(TParams *params, TParamsTelegraph *paramsTelegraph)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr = 0;
	SetDefaultParamsTelegraph(paramsTelegraph);


	// Telegraph Line
	// R1 - input load (Ohm)
	// R2 - output load (Ohm)
	// L - inductance (H, Henry)
	// C - capacitance (F, Farad)
	// om - omega (rad/s)

	ierr = PetscOptionsGetScalar(NULL,NULL,"-R1",&paramsTelegraph->R1,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(NULL,NULL,"-R2",&paramsTelegraph->R2,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(NULL,NULL,"-L",&paramsTelegraph->L,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(NULL,NULL,"-C",&paramsTelegraph->C,NULL);CHKERRQ(ierr);
	ierr = PetscOptionsGetScalar(NULL,NULL,"-om",&paramsTelegraph->om,NULL);CHKERRQ(ierr);

	// calculate the parameters - dt, tmax
	// size of the integration step
	params->dt = sqrt(paramsTelegraph->L*paramsTelegraph->C);

	// total propagation delay of the line
	PetscScalar tdelay = params->N*sqrt(paramsTelegraph->L*paramsTelegraph->C);

	// set the maximum simulation time
	if(paramsTelegraph->R1 == 100 && paramsTelegraph->R2 == 100)
	{// adjusted line
		params->tmax = 10*params->dt; //2*tdelay;//2e-8;
	}
	else if(paramsTelegraph->R1 == 100 && paramsTelegraph-> R2 == 1e12)
	{//amplified bounced
		params->tmax = 2*tdelay;//2e-8; // for voltageType = sinus, for puslse tmax = 4e-8;
	}

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Creates the path to the file
 * @param dir 		Dirctory
 * @param fname 	File name
 * @param path 		Resulting file path
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode CreateFilePath(char* dir, const char* fname, char* path)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr = 0;

	strcat(path, dir);
	strcat(path, "/");
	strcat(path, fname);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Loads matrix from MATLAB binary file.
 * @param fname_mat 	Binary file with matrix
 * @param AMat 			Output matrix with loaded data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode LoadBinMatrix(char* fname_mat, Mat* AMat)
{
	PetscFunctionBeginUser;

	PetscViewer    fd;
	PetscErrorCode ierr;

	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,fname_mat,FILE_MODE_READ,&fd);CHKERRQ(ierr);
	ierr = MatCreate(PETSC_COMM_WORLD,AMat);CHKERRQ(ierr);
	ierr = MatLoad(*AMat,fd);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&fd);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Loads vector from MATLAB binary file.
 * @param fname_mat 	Binary file with vector
 * @param 				Output vector with loaded data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode LoadBinVector(char* fname_vec, Vec* vVec)
{
	PetscFunctionBeginUser;

	PetscViewer    fd;
	PetscErrorCode ierr;

	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,fname_vec,FILE_MODE_READ,&fd);CHKERRQ(ierr);
	ierr = VecCreate(PETSC_COMM_WORLD,vVec);CHKERRQ(ierr);
	ierr = VecLoad(*vVec,fd);CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&fd);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Loads all necessary input data - matrix A, vectors ic, b.
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode LoadInputData(TParams *params, TData* data)
{
	PetscFunctionBeginUser;

	PetscErrorCode ierr;

	char path[PETSC_MAX_PATH_LEN] = "";
	ierr =  CreateFilePath(params->srcPath, "A.dat", path);CHKERRQ(ierr);
	ierr = LoadBinMatrix(path, &(data->A));CHKERRQ(ierr);

  	path[0] = '\0';
	ierr =  CreateFilePath(params->srcPath, "ic.dat", path);CHKERRQ(ierr);
	ierr = LoadBinVector(path, &(data->ic));CHKERRQ(ierr);

  	path[0] = '\0';
	ierr =  CreateFilePath(params->srcPath, "b.dat", path);CHKERRQ(ierr);
	ierr = LoadBinVector(path, &(data->b));CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Get information about matrix storage.
 * @param XMat 				Matrix
 * @param info 				Structure for storing matrix context
 * @param nz_used_percent 	Variable for storing number of nonzeroes
 * @param infoType 			Type of information about the local part of the matrix, the entire parallel matrix or the maximum over all the local parts
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode GetMatrixContext(Mat *XMat, MatInfo *info, PetscScalar *nz_used_percent, MatInfoType infoType)
{

	//MatInfo info_local; 		// context of matrix information (local matrix)
	//MatInfo info_global_max; 	// context of matrix information (maximum over all processors)
	//MatInfo info_global_sum; 	// context of matrix information (sum over all processors)

	PetscErrorCode ierr;
	PetscInt rows, cols;
	//MatInfo infoTmp;


	ierr = MatGetSize(*XMat, &rows, &cols);

	if(infoType == MAT_LOCAL)
	{
		MatGetInfo(*XMat, MAT_LOCAL, info);
	}
	else if(infoType == MAT_GLOBAL_MAX)
	{
		MatGetInfo(*XMat, MAT_GLOBAL_MAX, info);
	}
	else if(infoType == MAT_GLOBAL_SUM)
	{
		MatGetInfo(*XMat, MAT_GLOBAL_SUM, info);
	}
	else
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"GetMatrixContext: wrong MatInfoType!\n");CHKERRQ(ierr);
		PetscFunctionReturn(-1);
	}
	// calculate number of nonzeroes
	// 1.0 is just for retyping in the expression below
	// -> PetscLogDouble info->nz_used is retyped into PetscScalar thank to the denominator: (one*rows*rows) = PetscScalar * PetscInt * PetscInt
	*nz_used_percent = ((info->nz_used) / ((1.0*rows*rows)/100.0));

	//ierr = PetscPrintf(PETSC_COMM_WORLD,"GetMatrixContext: fill_ratio_given: %e,fill_ratio_needed: %e\n",info->fill_ratio_given, info->fill_ratio_needed);CHKERRQ(ierr);

	//ierr = PetscPrintf(PETSC_COMM_WORLD,"GetMatrixContext: %e = (%e / ((%d*%d)/100.0))\n", *nz_used_percent, info->nz_used, rows, rows);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Print information about matrix storage.
 * @param info 				Structure for storing matrix context
 * @param nz_used_percent 	Variable for storing number of nonzeroes
 * @param infoType 			Type of information about the local part of the matrix, the entire parallel matrix or the maximum over all the local parts
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintMatrixContext(MatInfo *info, PetscScalar *nz_used_percent, MatInfoType infoType, const char* infoStr)
{

	//MatInfo info_local; 		// context of matrix information (local matrix)
	//MatInfo info_global_max; 	// context of matrix information (maximum over all processors)
	//MatInfo info_global_sum; 	// context of matrix information (sum over all processors)

	PetscErrorCode ierr;
	MatInfo infoTmp;

	infoTmp = *info;


	if(infoType == MAT_LOCAL)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"LOCAL: Matrix %s\n", infoStr);CHKERRQ(ierr);
	}
	else if(infoType == MAT_GLOBAL_MAX)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"MAT_GLOBAL_MAX: Matrix %s\n", infoStr);CHKERRQ(ierr);
	}
	else if(infoType == MAT_GLOBAL_SUM)
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"MAT_GLOBAL_SUM: Matrix %s\n", infoStr);CHKERRQ(ierr);
	}
	else
	{
		ierr = PetscPrintf(PETSC_COMM_WORLD,"PrintMatrixContext: wrong MatInfoType!\n");CHKERRQ(ierr);
		PetscFunctionReturn(-1);
	}

	ierr = PetscPrintf(PETSC_COMM_WORLD,"block_size:%g\n", 		infoTmp.block_size);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"nz_allocated:%g\n", 	infoTmp.nz_allocated);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"nz_used:%g\n", 		infoTmp.nz_used);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"nz_unneeded:%g\n", 	infoTmp.nz_unneeded);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"memory:%g\n", 			infoTmp.memory);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"assemblies:%g\n", 		infoTmp.assemblies);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"mallocs:%g\n", 		infoTmp.mallocs);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"fill_ratio_given:%g\n", 		infoTmp.fill_ratio_given);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"fill_ratio_needed:%g\n", 		infoTmp.fill_ratio_needed);CHKERRQ(ierr);
	ierr = PetscPrintf(PETSC_COMM_WORLD,"factor_mallocs:%g\n", 			infoTmp.factor_mallocs);CHKERRQ(ierr);

	ierr = PetscPrintf(PETSC_COMM_WORLD,"nz_used_percent:%g\n\n", 		*nz_used_percent);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Opens a file for binary input/output.
 * Solves the system of ODEs numerically using explicit Taylor series method.
 * Definition of the problem: y' = A*y + b
 * where A is Jacobian matrix of constants and b is a vector of
 * constants and y is a vector [U;I]
 * @param params 			Structure with computational parameters
 * @param data 				Input data
 * @return 					Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode SetViewers(TParams *params)
{
	PetscFunctionBeginUser;
	PetscErrorCode 	ierr;

	// Y.dat
	char pathY[PETSC_MAX_PATH_LEN] = "";
	ierr =  CreateFilePath(params->dstPath, "Y.dat", pathY);CHKERRQ(ierr);

	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, pathY, FILE_MODE_WRITE, &(params->viewerY));CHKERRQ(ierr);
	ierr = PetscViewerBinarySetSkipHeader(params->viewerY,PETSC_TRUE);CHKERRQ(ierr);

	// ORD.dat
	char pathORD[PETSC_MAX_PATH_LEN] = "";
	ierr =  CreateFilePath(params->dstPath, "ORD.dat", pathORD);CHKERRQ(ierr);

	ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, pathORD, FILE_MODE_WRITE, &(params->viewerORD));CHKERRQ(ierr);
	ierr = PetscViewerBinarySetSkipHeader(params->viewerORD,PETSC_TRUE);CHKERRQ(ierr);

	#ifdef OUTPUT
		// T.dat
		char pathT[PETSC_MAX_PATH_LEN] = "";
		ierr =  CreateFilePath(params->dstPath, "T.dat", pathT);CHKERRQ(ierr);

		// T.dat
		ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, pathT, FILE_MODE_WRITE, &(params->viewerT));CHKERRQ(ierr);
		ierr = PetscViewerBinarySetSkipHeader(params->viewerT,PETSC_TRUE);CHKERRQ(ierr);
	#endif

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Destroys PETSc viewers.
 * @param params 				Parameters for the calculation
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode DestroyViwers(TParams *params)
{

	PetscFunctionBeginUser;
	PetscErrorCode 	ierr;

	// delete PETSc viewers
	ierr = PetscViewerDestroy(&(params->viewerY));CHKERRQ(ierr);
	ierr = PetscViewerDestroy(&(params->viewerORD));CHKERRQ(ierr);

	#ifdef OUTPUT
		ierr = PetscViewerDestroy(&(params->viewerT));CHKERRQ(ierr);
	#endif

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Destroys input data structures.
 * @param params 				Parameters for the calculation
 * @param data 					Input data
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode DestroyData(TParams *params, TData *data)
{

	PetscFunctionBeginUser;
	PetscErrorCode 	ierr;

	// delete data structures
	ierr = MatDestroy(&(data->A));CHKERRQ(ierr);
	ierr = VecDestroy(&(data->ic));CHKERRQ(ierr);
	ierr = VecDestroy(&(data->b));CHKERRQ(ierr);

	if (params->solver == S_MTSM_PRECALC)
	{
		ierr = MatDestroy(&(data->Aone));CHKERRQ(ierr);
		ierr = VecDestroy(&(data->bone));CHKERRQ(ierr);
	}

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Prints matrix information - sizes.
 * @param AMat 		Matrix in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintMatrixSize(Mat* AMat, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode ierr;
	PetscInt       rows,cols;     // matrix number of rows and cols

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	ierr = PetscPrintf(comm,"Matrix %s\n", infoStr);CHKERRQ(ierr);

	// size
	ierr = MatGetSize(*AMat,&rows,&cols);CHKERRQ(ierr);
	ierr = PetscPrintf(comm,"[%d %d]\n",rows, cols);CHKERRQ(ierr);


	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints matrix information - ownership.
 * @param AMat 		Matrix in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintMatrixOwnership(Mat* AMat, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode ierr;
	PetscInt       istart,iend;   // range of matrix rows owned by this processor
	PetscMPIInt    rank;

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

	ierr = PetscPrintf(comm,"Matrix %s\n", infoStr);CHKERRQ(ierr);

	// ownership
	ierr = MatGetOwnershipRange(*AMat,&istart,&iend);CHKERRQ(ierr);
	ierr = PetscSynchronizedPrintf(comm,"rank: %d istart = %d iend = %d\n",rank, istart, iend);CHKERRQ(ierr);
	ierr = PetscSynchronizedFlush(comm, PETSC_STDOUT);CHKERRQ(ierr);
	ierr = PetscPrintf(comm,"----------------\n");CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints matrix information - matrix content.
 * @param AMat 		Matrix in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintMatrix(Mat* AMat, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscViewer    viewer;
	PetscErrorCode ierr;

	// set viewers, communicators
	viewer = PETSC_VIEWER_STDOUT_WORLD;
	comm = PETSC_COMM_WORLD;

	// print matrix
	ierr = PetscPrintf(comm,"Matrix %s\n", infoStr);CHKERRQ(ierr);

	ierr = MatView(*AMat, viewer);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints matrix information - content, sizes, ownership.
 * @param AMat 		Matrix in PETSc format
 * @param infoStr 	Information caption
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintMatrixInfoAll(Mat* AMat, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode 	ierr;

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	// print matrix
	ierr = PetscPrintf(comm,"Matrix %s\n", infoStr);CHKERRQ(ierr);

	ierr = PrintMatrixSize(AMat, infoStr);CHKERRQ(ierr);
	ierr = PrintMatrixOwnership(AMat, infoStr);CHKERRQ(ierr);
	ierr = PrintMatrix(AMat, infoStr);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}


// ======================================================================
/**
 * Prints vector information - sizes.
 * @param vVec Vector in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintVectorSize(Vec* vVec, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode ierr;
	PetscInt       cols;     		// matrix number of rows and cols

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	ierr = PetscPrintf(comm,"Vector %s\n", infoStr);CHKERRQ(ierr);

	// size
	ierr = VecGetSize(*vVec,&cols);CHKERRQ(ierr);
	ierr = PetscPrintf(comm,"[%d]\n",cols);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints vector information - ownership.
 * @param vVec Vector in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintVectorOwnership(Vec* vVec, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode ierr;
	PetscInt       istart,iend;   	// range of matrix rows owned by this processor
	PetscMPIInt    rank;

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	// get rank of the process
	ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

	ierr = PetscPrintf(comm,"Vector %s\n", infoStr);CHKERRQ(ierr);

	// ownership
	ierr = VecGetOwnershipRange(*vVec,&istart,&iend);CHKERRQ(ierr);
	ierr = PetscSynchronizedPrintf(comm,"rank: %d istart = %d iend = %d\n",rank, istart, iend);CHKERRQ(ierr);
	ierr = PetscSynchronizedFlush(comm, PETSC_STDOUT);CHKERRQ(ierr);
	ierr = PetscPrintf(comm,"----------------\n");CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints vector information - content, sizes, ownership.
 * @param vVec Vector in PETSc format
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintVector(Vec* vVec, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscViewer    viewer;
	PetscErrorCode ierr;

	// set viewers, communicators
	viewer = PETSC_VIEWER_STDOUT_WORLD;
	comm = PETSC_COMM_WORLD;

	ierr = PetscPrintf(comm,"Vector %s\n", infoStr);CHKERRQ(ierr);

	// print vector
	ierr = VecView(*vVec, viewer);CHKERRQ(ierr);


	PetscFunctionReturn(ierr);
}

// ======================================================================
/**
 * Prints vector information - content, sizes, ownership.
 * @param vVec Vector in PETSc format
 * @param infoStr Information caption
 * @return Returns 0 if everything is OK, error code otherwise
 */
PetscErrorCode PrintVectorInfoAll(Vec* vVec, const char* infoStr)
{
	PetscFunctionBeginUser;

	MPI_Comm       comm;
	PetscErrorCode 	ierr;

	// set viewers, communicators
	comm = PETSC_COMM_WORLD;

	ierr = PetscPrintf(comm,"Vector %s\n", infoStr);CHKERRQ(ierr);

	ierr = PrintVectorSize(vVec, infoStr);CHKERRQ(ierr);
	ierr = PrintVectorOwnership(vVec, infoStr);CHKERRQ(ierr);
	ierr = PrintVector(vVec, infoStr);CHKERRQ(ierr);

	PetscFunctionReturn(ierr);
}
