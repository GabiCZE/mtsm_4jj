# Modern Taylor Series Method
* Parallel implementation of Modern Taylor Series Method (MTSM) using [PETSc](https://www.mcs.anl.gov/petsc/) library 

## Requirements

* PETSc library (I am using: petsc/3.13.5_gcc-10.1.0)
* OpenMPI (I am using: openmpi/ucx-4.0.5-gcc-10.1.0)

## Usage

```sh
make base       # compile the base.c
cd sbatchDir
sh run_this.sh  # submits individual jobs 
```

## Source file description
### base.c
* PETSc implementation 
* Input: matrix-vector representation of linear system of ODEs, supposes binary files (A.dat, ic.dat, b.dat) 
* Output: 
    * `TIME` mode (set in Makefile)
        * prints results to stdout: `solver,N:%d,proc:%d,time:%e,steps:%d,precalc-time:%e,comp-time:%e,avg-ORD:%d,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f,glob_sum_nz_used_percent:%f,glob_sum_memory:%f`
        * saves results in the last integration step: `experiments/PROBLEM_NAME_<params>/results/N/numProc/solver/Y.dat` 
            * Note: MTSM solver - the average ORD is saved (ORD.dat)  
    *  `OUTPUT` mode (set in Makefile)
        * saves results in all integration steps: `experiments/PROBLEM_NAME_<params>/results/N/numProc/solver/{Y, T}.dat` (MTSM: also `ORD.dat`)
    *  `DEBUG` mode (set in Makefile)  
        * print debug messages (e.g. results for each integration steps) 
    *  `LOGVIEW` mode (set in Makefile)  
        * Experimental: register routines for code profiling       

#### Basic parameters (base)

* `-N` - size of the problem 
* `-eps` - accuracy for MTSM solvers
* `-eps_rk` - accuracy for RK solvers
* `-dt` - size of the integration step 
* `-tmax` - maximum simulation time
* `maxORD` - maximal order of MTSM
* `-pType` - problem type 
    * `PT_TELEGRAPH` - "telegraph"
    * `PT_WAVE_FSFO_combined`  - "wave" (1D), possibly other types
    * `PT_HEAT1D_SIN` - "heat2D_sin"
* `-solType` - solver type
    * `ST_TAYLOR`  - "taylor" 
    * `maxORD` - maximum order of the MTSM
    * `ST_RK` - "rk"
* `-solver` - solver  
    * `S_MTSM` (classical method)
    * `S_MTSM_PRECALC` (precalculated matrix A)
    * *RK solvers with embedded functions*
    * `S_TSRK3BS` (ode23)
    * `S_TSRK5DP` (ode45)
    * `S_TSRK5F`
    * `S_TSRK5BS`
    * *new RK solvers*
    * `TSRK6VR`
    * `TSRK7VR`
    * `TSRK8VR`
    * *RK solvers without embedded functions*
    * `S_TSRK1FE`
    * `S_TSRK2A`
    * `S_TSRK3`
    * `S_TSRK4`
* `-srcPath` - path to the source data (matrix A, vectors ic, b)
* `-dstPath` - path to the results (comparison results, results from the last step, calculation times, graphs)
* `-mode` - program mode ("debug" | "print")

#### Other parameters

Special parameters are only for Telegraph Line Equation, because based on these parameters, the `dt` and `tmax` are calculated

* `-R1` - input load (Ohm)
* `-R2` - output load (Ohm)
* `-L` - inductance (H, Henry)
* `-C` - capacitance (F, Farad)
* `-om` - omega (rad/s)

### datatypes.[c|h]
* Contains data structures and enums

### general.[c|h]
* Contains basic functions
    * generate matrix for MTSM_PRECALC
    * get/print parameters
    * viewer settings
    * load input data
    * get matrix context
    * print matrix/vector information


### Makefile

* Modes: `TIME`, `OUTPUT`, `DEBUG`, `LOGVIEW` (profiling)
* See description in `base.c`


### sbatchDir folder

* Contains sbatch scripts generated according to the JSON file:
    * `nodes_X.sh` sbatch script for running experiment on `X` nodes 
        * for given problem sizes and number of processes and number of runs, it calls `mpirun -np numProc ./base.c {params}`
        * the information are printed to stdout in this format: 
        * `solver,N:%d,proc:%d,time:%e,steps:%d,precalc-time:%e,comp-time:%e,avg-ORD:%d,Nreal:%d,glob_sum_nz_alloc:%f,glob_sum_nz_used:%f, glob_sum_nz_used_percent:%f,glob_sum_memory:%f\n`   
        * every solver runs this sbatch script  
    * `run_this.sh` bash script that call `nodes_X.sh` sbatch scripts for given number of nodes and given solvers
* Output: `sbatchDir/nodes_X.sh`, `sbatchDir/run_this.sh`, `jobIDs_<timestamp>.jobs` with jobID-solver mapping


