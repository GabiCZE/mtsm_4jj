# Makefile
# "-" means ignore the exit status of the command that is executed (normally, a non-zero exit status would stop that part of the build).

CFLAGS += -DTIME
#CFLAGS += -DLOGVIEW
#CFLAGS += -DOUTPUT
#CFLAGS += -DDEBUG

OBJFILES = datatypes.o general.o base.o

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

TARGET=base

all: $(TARGET)

%.o: %.c %.h
	${PETSC_COMPILE} -c -o $@  $<

$(TARGET): $(OBJFILES)
	-${CLINKER} -o $@ $(OBJFILES) ${PETSC_SYS_LIB}
	rm -f $(OBJFILES)


c: clc
clc:
	rm -f $(TARGET) *.o
