#!/bin/bash -l
# Generated 2021-07-04_22-04-18

namefile=jobIDs_$(date "+%Y-%m-%d_%H-%M-%S").jobs
echo $namefile > lastjobs.tmp

if [ ! -d "../data/telegraph_simple" ] ; then
	echo "Error: ../data/telegraph_simple does not exist!"
	exit 1
fi

for num in 1 2 ; do
	for solver in "MTSM" "MTSM_PRECALC" "MTSM_O2" ; do
		job=$(sbatch --job-name=$solver nodes_$num.sh "taylor" $solver)
		jobid=$(echo $job | cut -d' ' -f4)
		echo $solver $jobid >> $namefile
	done

	for solver in "TSRK5DP" "TSRK8VR" ; do
		job=$(sbatch --job-name=$solver nodes_$num.sh "rk" $solver)
		jobid=$(echo $job | cut -d' ' -f4)
		echo $solver $jobid >> $namefile
	done
done

sleep 1
squeue -u $(whoami)
