#!/bin/bash -l
# Generated 2021-06-27_17-28-09

#SBATCH --time=2-00:00:00
#SBATCH --mem=64000
#SBATCH -N 1
#SBATCH --ntasks-per-node=16
#SBATCH --exclusive
#SBATCH --output=%j.out
#SBATCH --error=%j.err


# load modules
module load use.own
module load petsc/3.13.5_gcc-10.1.0
#----------------------------------------------------------
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

solverType=$1
solver=$2
nEqs=32000


# print header of the CSV file
echo "n;numProc;numRuns;time;numSteps;precalc-time;calc-time;avgORD;nreal;glob_sum_nz_alloc;glob_sum_nz_used;glob_sum_nz_used_percent;glob_sum_memory"


# number of segments
for (( s=1; s<=1; s++ ))
do
	# number of processes (how many fork(), 1,2,4,8,16,32,64,128)
	for numProc in 16
	do
		srcPathFinal="../data/telegraph_simple/$nEqs"
		if [ ! -d "$srcPathFinal" ] ; then	# if directory not exist
			echo "Missing input data in $srcPathFinal" >> /dev/stderr
			exit 1
		fi

		dstPathFinal="../experiments/telegraph_simple_32000_32_1_2021-06-27_17-28-09/results/$nEqs/$numProc/$solver"
		mkdir -p $dstPathFinal

		currTime_num=0
		currTime_sum=0

		# number of runs
		for (( r=1; r<=1; r++ ))
		do
			# run PETSc programm
			time_measured=$(mpirun -np $numProc .././base -N $nEqs -solType $solverType -solver $solver -pType telegraph_simple -srcPath $srcPathFinal -dstPath $dstPathFinal -mode solve -eps_rk 1e-10 -eps 1e-10 -dt -1 -tmax -1 -maxORD 64)


			echo $time_measured >> telegraph_32000_simple_JJ_2021-06-27_17-28-09.log
			currTime_str=$(echo $time_measured | cut -d',' -f4 | cut -d':' -f2)

			currTime_num=$(printf '%.15f' $currTime_str)   # precision: 15 dec. places
			currTime_sum=$(echo "$currTime_sum+$currTime_num" | bc -l)
			currTime_num=0
			numSteps=$(echo $time_measured | cut -d',' -f5 | cut -d':' -f2)
			numSteps=$(($numSteps-1))
			precalc_time=$(echo $time_measured | cut -d',' -f6 | cut -d':' -f2)
			calc_time=$(echo $time_measured | cut -d',' -f7 | cut -d':' -f2)
			avg_ORD=$(echo $time_measured | cut -d',' -f8 | cut -d':' -f2)
			Nreal=$(echo $time_measured | cut -d',' -f9 | cut -d':' -f2)
			glob_sum_nz_alloc=$(echo $time_measured | cut -d',' -f10 | cut -d':' -f2)
			glob_sum_nz_used=$(echo $time_measured | cut -d',' -f11 | cut -d':' -f2)
			glob_sum_nz_used_percent=$(echo $time_measured | cut -d',' -f12 | cut -d':' -f2)
			glob_sum_memory=$(echo $time_measured | cut -d',' -f13 | cut -d':' -f2)

		done

		avgtime_measured=$(echo "$currTime_sum/1" | bc -l)
		echo "$nEqs;$numProc;1;$avgtime_measured;$numSteps;$precalc_time;$calc_time;$avg_ORD;$Nreal;$glob_sum_nz_alloc;$glob_sum_nz_used;$glob_sum_nz_used_percent;$glob_sum_memory"
	done

	nEqs=$((nEqs*2))

done
