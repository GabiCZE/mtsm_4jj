/**
 * Project: Parallel solution of PDEs using Method of Lines (MOL)
 * The MOL discretizes one dimension and then integrates the semi-discrete problem as a system of ODEs.
 * The resulting system of ODEs can be solved using standard numerical methods - MTSM and RK solvers.
 * File: base.h
 * Author: Gabriela Necasova, inecasova@fit.vutbr.cz
 * Date: August 2018
 * Modification: November 2020
 */

#pragma once


/**
 * Problem type - string format
 */
extern const char *PROBLEM_TYPE_STR[];

/**
 * Solver type - string format
 */
extern const char *SOLVER_TYPE_STR[];

/**
 * Program mode - string format
 */
extern const char *MODE_STR[];

/**
 * Solver name - string format
 */
extern const char *SOLVER_STR[];

/**
 * Program mode
 */
enum MODE
{
	SOLVE = 0,
	PRINT
};


/**
 * Solver type
 */
enum SOLVER_TYPE
{
	ST_TAYLOR = 0,
	ST_RK
};


/**
 * Problem type
 */
enum PROBLEM_TYPE
{
	PT_TELEGRAPH_SIMPLE = 0,
	PT_TELEGRAPH_FULL,
	PT_STOKES,
	PT_WAVE_FSFO_combined,
	PT_WAVE_FSFO_symmetric_ghost,
	PT_WAVE_FSFO_symmetric_noGhost,
	PT_WAVE_FSVO_symmetric,
	PT_LAPLACE_2D,
	PT_HEAT2D_SIN,
	PT_HEAT1D_SIN,
	PT_SINCOS

};


/**
 * Solver name
 */
enum SOLVER
{
	// Modern Taylor Series Method
	S_MTSM = 0,
	S_MTSM_O2,
	S_MTSM_PRECALC,

	// RK with embedded method
	S_TSRK3BS,
	S_TSRK5DP,
	S_TSRK5F,
	S_TSRK5BS,
	// RK new solvers
	S_TSRK6VR,
	S_TSRK7VR,
	S_TSRK8VR,

	// RK without embedded method
	S_TSRK1FE,
	S_TSRK2A,
	S_TSRK3,
	S_TSRK4
};


/**
 * Structure for command line parameters
 */
typedef struct params
{
	PetscInt N;				// problem size
	PetscScalar eps; 		// precision
	PetscScalar eps_rk; 	// precision for RK solvers
	PetscScalar dt;  		// integration step
	PetscScalar tmax; 		// maximum simulation time
	PetscInt maxORD; 		// maximal order of the MTSM

	int problemType; 		// type of the problem (PTYPE_TELEGRAPH -> "telegraph", etc.)
	int solverType; 		// type of the solver (STYPE_TAYLOR -> "mtsm", STYPE_RK -> "rk", etc.)
	int solver; 			// specific solver (depends on the solverType)
	char srcPath[PETSC_MAX_PATH_LEN]; 			// source folder for loading input data
	char dstPath[PETSC_MAX_PATH_LEN]; 			// destination folder for saving results

	int mode; 	// program mode (SOLVE/PRINT)

	PetscViewer 	viewerORD;	// for MATLAB (writing to the bin file - vector of orderd of the method)
	PetscViewer 	viewerY;	// for MATLAB (writing to the bin file - solution Y)
	PetscViewer 	viewerT;	// for MATLAB (writing to the bin file - vector of simulation times T)

} TParams;


/**
 * Structure for parameters specific for the Telegraph equation
 */
typedef struct paramsTelegraph
{
	PetscScalar R1;			// input load (Ohm)
	PetscScalar R2;			// output load (Ohm)
	PetscScalar L;			// inductance (H, Henry)
	PetscScalar C;			// capacitance (F, Farad)
	PetscScalar om;			// omega (rad/s)

} TParamsTelegraph;


/**
 * Structure for input data
 */
typedef struct data
{
	Mat A; 		// matrix A
	Vec ic;  	// vector with initial conditions
	Vec b; 		// vector b

	Mat Aone;	// matrix Aone - precalculted matrix A from order 1 to maxORD
	Vec bone; 	// vector bone - precalculted vector b from order 1 to maxORD
	PetscLogDouble Aone_generation_time; // matrix A generation time

	// link: https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatInfo.html#MatInfo
	// enum {MAT_LOCAL=1,MAT_GLOBAL_MAX=2,MAT_GLOBAL_SUM=3} MatInfoType;

	// local matrix (MAT_LOCAL)
	MatInfo info_local; 				// context of matrix information
	PetscScalar local_nz_used_percent;	// number of zeroes of the matrix A (%)

	// maximum over all processors (MAT_GLOBAL_MAX)
	MatInfo info_global_max; 				// context of matrix information
	PetscScalar glob_max_nz_used_percent; 	// number of zeroes of the matrix A (%)

	// sum over all processors (MAT_GLOBAL_SUM)
	MatInfo info_global_sum; 				// context of matrix information (sum over all processors)
	PetscScalar glob_sum_nz_used_percent; 	// number of zeroes of the matrix A (%)

	PetscInt data_size; 	// real size of the problem (matrix A) obtained by MatGetSize

} TData;


/**
 * User-defined context
 */
typedef struct
{
	char* path;
	PetscViewer 	viewerY;	// for MATLAB (writing to the bin file - solution Y)
	PetscViewer 	viewerT;	// for MATLAB (writing to the bin file - vector of simulation times T)
	Vec T;
	Mat A;
	Vec b;

	PetscInt steps;
}AppCtx;


