#include <petscksp.h>
#include "datatypes.h"

// https://stackoverflow.com/questions/30933551/how-can-i-share-a-const-char-array-between-two-source-files-gracefully

/**
 * Problem type - string format
 */
const char *PROBLEM_TYPE_STR[] =
{
	"telegraph_simple",
	"telegraph_full",
	"stokes",
	"wave_FSFO_combined",
	"wave_FSFO_symmetric_ghost",
	"wave_FSFO_symmetric_noGhost",
	"wave_FSVO_symmetric",
	"laplace2D",
	"heat2D_sin",
	"heat1D_sin",
	"sincos"
};


/**
 * Solver type - string format
 */
const char *SOLVER_TYPE_STR[] =
{
	"taylor",
	"rk",
};


/**
 * Program mode - string format
 */
const char *MODE_STR[] =
{
	"solve",
	"print",
};


/**
 * Solver name - string format
 */
const char *SOLVER_STR[] =
{
	// Modern Taylor Series Method
	"MTSM",

	// Modern Taylor Series Method - optimization of stopping rule
	// - only the last Taylor series term
	"MTSM_O2",

	// Modern Taylor Series Method - precalculated matrix
	"MTSM_PRECALC",

	// RK with embedded method
	"TSRK3BS",
	"TSRK5DP",
	"TSRK5F",
	"TSRK5BS",
	// RK new solvers
	"TSRK6VR",
	"TSRK7VR",
	"TSRK8VR",

	// RK without embedded method
	"TSRK1FE",
	"TSRK2A",
	"TSRK3",
	"TSRK4",
};
