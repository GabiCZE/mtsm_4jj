/**
 * Project: Parallel solution of PDEs using Method of Lines (MOL)
 * The MOL discretizes one dimension and then integrates the semi-discrete problem as a system of ODEs.
 * The resulting system of ODEs can be solved using standard numerical methods - MTSM and RK solvers.
 * File: general.h
 * Author: Gabriela Necasova, inecasova@fit.vutbr.cz
 * Date: August 2018
 * Modification: November 2020
 */

#pragma once

#include "datatypes.h"

// generate input matrix (MTSM_PRECALC)
PetscErrorCode GenerateInputs_one_mtx(TParams *params, TData *data);
PetscErrorCode GenerateInputs_one_mtx_all(TParams *params, TData *data);

// get params
PetscErrorCode GetParams(TParams *params);
void SetDefaultParamsTelegraph(TParamsTelegraph *paramsTelegraph);
PetscErrorCode GetParamsTelegraph(TParams *params, TParamsTelegraph *paramsTelegraph);

// print params
PetscErrorCode PrintParams(TParams *params);
PetscErrorCode PrintParamsTelegraph(TParamsTelegraph *paramsTelegraph);

// viewers settings
PetscErrorCode SetViewers(TParams *params);
PetscErrorCode DestroyViwers(TParams *params);
PetscErrorCode DestroyData(TParams *params, TData *data);

// load input data
PetscErrorCode CreateFilePath(char* dir, const char* fname, char* path);
PetscErrorCode LoadBinMatrix(char* fname_mat, Mat* AMat);
PetscErrorCode LoadBinVector(char* fname_vec, Vec* vVec);
PetscErrorCode LoadInputData(TParams *params, TData* data);

// matrix context
PetscErrorCode GetMatrixContext(Mat *XMat, MatInfo *info, PetscScalar *nz_used_percent, MatInfoType infoType);
PetscErrorCode PrintMatrixContext(MatInfo *info, PetscScalar *nz_used_percent, MatInfoType infoType, const char* infoStr);

// matrix info - size, ownership, content, all
PetscErrorCode PrintMatrixSize(Mat* AMat, const char* infoStr);
PetscErrorCode PrintMatrixOwnership(Mat* AMat, const char* infoStr);
PetscErrorCode PrintMatrix(Mat* AMat, const char* infoStr);

PetscErrorCode PrintMatrixInfoAll(Mat* AMat, const char* infoStr);

// vector info - size, ownership, content, all
PetscErrorCode PrintVectorSize(Vec* vVec, const char* infoStr);
PetscErrorCode PrintVectorOwnership(Vec* vVec, const char* infoStr);
PetscErrorCode PrintVector(Vec* vVec, const char* infoStr);

PetscErrorCode PrintVectorInfoAll(Vec* vVec, const char* infoStr);
